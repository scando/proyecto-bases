package Tester;


import Clases.InterfaceMethods.ConectorBDD;
import Clases.Util.LookAndFeelClass;
import Controlador.ControladorConectar;
import InterfacesAdministrador.Preferencias;
import InterfacesAdministrador.vtnPrincipalAdm;
import InterfacesUsuario.*;
import java.sql.SQLException;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Clases
 */
//
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*
        ConectorBDD conector = ConectorBDD.getInstance();
        ControladorConectar con = ControladorConectar.getInstance();
        try {
            con.listaTablas();
        } catch (SQLException ex) {
            System.out.println("Error:\n" + ex.getMessage());
        }
*/
        
        VtnBienvenido vtn = new VtnBienvenido();
        vtn.setVisible(false);
        vtnAgregarComida vtn2 = new vtnAgregarComida();
        vtn2.setVisible(false);
        vtnCrearUsuario user = new vtnCrearUsuario(vtn2);
        user.setVisible(false);
        
        LookAndFeelClass.lookAndFeelNimbus();
        vtnPrincipalAdm adm = vtnPrincipalAdm.getInstance();
        adm.setVisible(true);
        
        /*
        ConectorBDD model = ConectorBDD.getInstance();
        Preferencias view = new Preferencias(new javax.swing.JFrame(), true);
        ControladorConectar.setView(view);
        ControladorConectar.getInstance();
        view.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                System.exit(0);
            }
        });
        view.setVisible(true);
        
        */
    }
}
