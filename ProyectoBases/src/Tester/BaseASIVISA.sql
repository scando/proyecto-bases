CREATE database BaseASIVISA;
USE BaseASIVISA;

CREATE TABLE IntensidadEjercicio(
	idIntensidad INT NOT NULL AUTO_INCREMENT,
	intensidad VARCHAR (15) UNIQUE NOT NULL,
	PRIMARY KEY (idIntensidad)
);

CREATE TABLE Ejercicio(
	idEjercicio INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR (100) UNIQUE NOT NULL,
	descripcion VARCHAR (200) NOT NULL,
	intensidad INT NOT NULL,
	calorias INT NOT NULL,
	PRIMARY KEY (idEjercicio),
	FOREIGN KEY (intensidad)
		REFERENCES IntensidadEjercicio(idIntensidad)
		ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE Rutina(
	idRutina INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR (50) UNIQUE NOT NULL,
	descripcion VARCHAR (200) NOT NULL,
	PRIMARY KEY (idRutina)
);

CREATE TABLE EjercicioRutina(
	ejercicio INT NOT NULL , 
	rutina INT NOT NULL,
	FOREIGN KEY (ejercicio)
		REFERENCES Ejercicio(idEjercicio)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (rutina)
		REFERENCES Rutina(idRutina)
		ON UPDATE CASCADE ON DELETE CASCADE,
	PRIMARY KEY (ejercicio, rutina)
);

CREATE TABLE AreaDelCuerpo(
	idADC INT AUTO_INCREMENT,
	nombre VARCHAR (20) UNIQUE NOT NULL,
	PRIMARY KEY (idADC)
);

CREATE TABLE EjercicioArea(
	ejercicio INT,
	area int,
	FOREIGN KEY (ejercicio)
		REFERENCES Ejercicio(idEjercicio)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (area)
		REFERENCES AreaDelCuerpo(idADC)
		ON UPDATE CASCADE ON DELETE CASCADE,
	PRIMARY KEY (ejercicio, area)
);

CREATE TABLE TipoMembresia(
	idTipoMembresia INT NOT NULL AUTO_INCREMENT,
	nombreTipoMembresia VARCHAR(20) UNIQUE NOT NULL,
	diasVigencia INT NOT NULL,
	costoMembresia DECIMAL NOT NULL,
	PRIMARY KEY (idTipoMembresia)
);

CREATE TABLE Usuario(
	idUsuario INT AUTO_INCREMENT,
	nombre VARCHAR (50) NOT NULL,
	cedula VARCHAR(20) UNIQUE NOT NULL,
	username VARCHAR(20) UNIQUE NOT NULL,
	contrasena VARCHAR(41) NOT NULL,
	email VARCHAR (30) UNIQUE NOT NULL,
	residencia VARCHAR (50) NOT NULL,
	pais VARCHAR (30) NOT NULL,
	ciudad VARCHAR (30) NOT NULL,
	pesoInicial INT NOT NULL,
	pesoActual INT NOT NULL, 
	pesoMeta INT NOT NULL,
	estatura INT NOT NULL,
	perfilPublico boolean NOT NULL DEFAULT TRUE,
	tipoMembresia INT,
	fechaInicioMembresia DATE NOT NULL,
	fechaFinMembresia DATE NOT NULL,
	PRIMARY KEY (idUsuario),
	FOREIGN KEY (tipoMembresia)
		REFERENCES TipoMembresia(idtipoMembresia)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Dieta(
	idDieta INT AUTO_INCREMENT,
	nombre VARCHAR(30) UNIQUE NOT NULL,
	ingestaCalorica INT NOT NULL DEFAULT 0,
	PRIMARY KEY (idDieta)
);

CREATE TABLE RutinaUsuario(
	idRutinaUsuario INT NOT NULL AUTO_INCREMENT,
	rutina INT NOT NULL,
	usuario INT NOT NULL,
	FechaInicio DATE NOT NULL,
	FechaFin DATE NOT NULL,
	FOREIGN KEY (usuario)
		REFERENCES Usuario(idUsuario)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (rutina)
		REFERENCES Rutina(idRutina)
		ON UPDATE CASCADE ON DELETE CASCADE,
	PRIMARY KEY (idRutinaUsuario)
);

CREATE TABLE UsuarioDieta(
	idUsuarioDieta INT NOT NULL AUTO_INCREMENT,
	dieta INT NOT NULL,
	usuario INT NOT NULL,
	fechaInicio Date NOT NULL,
	fechaFin Date NOT NULL,
	FOREIGN KEY (dieta)
		REFERENCES Dieta(idDieta)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (usuario)
		REFERENCES Usuario(idUsuario)
		ON UPDATE CASCADE ON DELETE CASCADE,
	PRIMARY KEY (idUsuarioDieta)
);

CREATE TABLE HoraComida(
	idhoraComida INT NOT NULL AUTO_INCREMENT,
	hora VARCHAR (20) NOT NULL,
	PRIMARY KEY (idhoraComida)
);

CREATE TABLE Comida(
	idComida INT NOT NULL AUTO_INCREMENT,
	calorias INT NOT NULL,
	nombre VARCHAR (20) NOT NULL,
	descripcion VARCHAR (100) NOT NULL,
	platoFuerte BOOLEAN NOT NULL,
	hora INT NOT NULL,
	PRIMARY KEY (idComida),
	FOREIGN KEY (hora)
		REFERENCES HoraComida(idhoraComida)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE ComidaDieta(
	comida INT,
	dieta INT,
	FOREIGN KEY (comida)
		REFERENCES Comida(idComida)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (dieta)
		REFERENCES Dieta(idDieta)
		ON UPDATE CASCADE ON DELETE CASCADE,
	PRIMARY KEY (comida, dieta)
);

CREATE TABLE Factura(
	idFactura INT AUTO_INCREMENT,
	fechaEmision DATE NOT NULL,
	fechaCredito DATE NOT NULL,
	monto DECIMAL NOT NULL,
	usuario INT NOT NULL,
	valido BOOLEAN NOT NULL DEFAULT TRUE,
	saldo INT NOT NULL DEFAULT 0,
	FOREIGN KEY (usuario)
		REFERENCES Usuario(idUsuario)
		ON UPDATE CASCADE ON DELETE CASCADE,
	PRIMARY KEY (idFactura)
);

CREATE TABLE Pago (
    idPago INT AUTO_INCREMENT,
    fecha DATE NOT NULL,
    factura INT NOT NULL,
    descripcion VARCHAR(50) NOT NULL,
    monto decimal NOT NULL,
	valido boolean NOT NULL DEFAULT TRUE,
    PRIMARY KEY (idPago),
    FOREIGN KEY (factura)
        REFERENCES Factura (idFactura)
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE USER ASIVISA IDENTIFIED BY 'asivisa';
GRANT ALL ON BaseASIVISA.* to ASIVISA;
