
use BaseASIVISA;

#---------------------------------------------------------------------------------------#
#Procedimientos AREADELCUERPO

DROP PROCEDURE IF EXISTS grabarADC;
DELIMITER //
CREATE PROCEDURE `grabarADC` (IN id INT, IN nombreADC VARCHAR(20))
#SP para grabar en la tabla AreaDelCuerpo
BEGIN
	IF id = 0 THEN
		INSERT INTO areadelcuerpo (nombre) VALUES(nombreADC);
		SELECT LAST_INSERT_ID();
	ELSE
		UPDATE areadelcuerpo SET nombre=nombreADC WHERE idADC=id;		
	END IF;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarADC;
DELIMITER //
CREATE PROCEDURE `cargarADC` (in id int)
#SP para cargar desde la tabla AreaDelCuerpo
BEGIN
	SELECT * FROM AreaDelCuerpo WHERE idADC=id;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarADCs;
DELIMITER //
CREATE PROCEDURE `cargarADCs` ()
#SP para cargar desde la tabla AreaDelCuerpo
BEGIN
	SELECT * FROM AreaDelCuerpo;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS eliminarADC;
DELIMITER //
CREATE PROCEDURE `eliminarADC` (IN id INT)
#SP para eliminar un registro en la tabla AreaDelCuerpo
BEGIN
	DELETE FROM AreaDelCuerpo WHERE idADC=id;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS buscarADCNombre;
DELIMITER //
CREATE PROCEDURE `buscarADCNombre` (IN nom varchar(20))
#SP para eliminar un registro en la tabla AreaDelCuerpo
BEGIN
	select * FROM AreaDelCuerpo 
		WHERE nombre like concat('%', nom, '%');
END
//
DELIMITER ;

#---------------------------------------------------------------------------------------#
#Procedimientos INTENSIDADEJERCICIO

DROP PROCEDURE IF EXISTS grabarIntensidad;
DELIMITER //
CREATE PROCEDURE `grabarIntensidad` (IN id INT, IN nombreIntensidad VARCHAR(20))
#SP para grabar en la tabla IntensidadEjercicio
BEGIN
	IF id = 0 THEN
		INSERT INTO IntensidadEjercicio (nombre) VALUES(nombreIntensidad);
		SELECT LAST_INSERT_ID();
	ELSE
		UPDATE IntensidadEjercicio SET nombre=nombreIntensidad WHERE idIntensidad=id;		
	END IF;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarIntensidades;
DELIMITER //
CREATE PROCEDURE `cargarIntensidades` ()
#SP para cargar desde la tabla IntensidadEjercicio
BEGIN
	SELECT * FROM IntensidadEjercicio;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarIntensidad;
DELIMITER //
CREATE PROCEDURE `cargarIntensidad` (in id int)
#SP para cargar desde la tabla IntensidadEjercicio
BEGIN
	SELECT intensidad FROM IntensidadEjercicio where idIntensidad=id;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS eliminarIntensidad;
DELIMITER //
CREATE PROCEDURE `eliminarIntensidad` (IN id INT)
#SP para eliminar un registro en la tabla IntensidadEjercicio
BEGIN
	DELETE FROM IntensidadEjercicio WHERE idIntensidad=id;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS busquedaIntensidad;
DELIMITER //
CREATE PROCEDURE `busquedaIntensidad` (IN nom varchar(20))
#SP para eliminar un registro en la tabla IntensidadEjercicio
BEGIN
	select * FROM IntensidadEjercicio 
		WHERE nombre like concat('%',nom,'%');
END
//
DELIMITER ;


#---------------------------------------------------------------------------------------#
#Procedimientos USUARIO

DROP PROCEDURE IF EXISTS grabarUsuario;
DELIMITER //
CREATE PROCEDURE grabarUsuario(IN id INT, IN nom VARCHAR(39), IN ced VARCHAR(20), IN usrname VARCHAR(20), IN contr VARCHAR(20), 
								IN emailu VARCHAR(30), IN resid varchar(30), in pai varchar(30), in ciu varchar(30), 
								in pesoI int, in pesoA int, in pesoM int, in estat int, in perfilP tinyint(1), in tipoM int )
BEGIN
	IF id = 0 THEN 
		Insert into Usuario (nombre, cedula, username, contrasena, email, residencia, pais, ciudad, pesoInicial, pesoActual, pesoMeta,
								 estatura, perfilPublico, tipoMembresia, fechaInicioMembresia, fechaFinMembresia)
			VALUES(nom, ced, usrname, password(contr), emailu, resid, pai, ciu, pesoI, pesoA, pesoM, estat, perfilP, tipoM, current_date, current_date);
		SELECT LAST_INSERT_ID(); 
	ELSE 
		UPDATE Usuario SET nombre=nom, cedula=ced, username=usrname, contrasena=password(contr), email=emailu, residencia=resid, pais=pai, 
							ciudad=ciu, pesoInicial=pesoI, pesoActual=pesoA, pesoMeta=pesoM, estatura=estat, perfilPublico=perfilP,
							tipoMembresia=tipoM
			WHERE idUsuario = id;
	END IF;
  
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS eliminarUsuario;
DELIMITER //
CREATE PROCEDURE eliminarUsuario(IN id int)
BEGIN
	delete from Usuario
	where idUsuario=id;
  
END //
DELIMITER ;


#Procedimiento de la clase UsuarioBusqueda
DROP PROCEDURE IF EXISTS busquedaUsuarioNombre;
DELIMITER // 
CREATE PROCEDURE busquedaUsuarioNombre(IN nombreU varchar(200))
BEGIN	
	select *
	from Usuario
	where	nombre like concat('%', nombreU, '%');
END //
DELIMITER ; 

#Procedimiento de la clase UsuarioBusqueda
DROP PROCEDURE IF EXISTS busquedaUsuarioEstatura;
DELIMITER // 
CREATE PROCEDURE busquedaUsuarioEstatura(IN estaturaU float)
BEGIN
	select *
	from Usuario
	where estatura=estaturaU;
  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS busquedaUsuarioPesoInicial;
#Procedimiento de la clase UsuarioBusqueda
DELIMITER // 
CREATE PROCEDURE busquedaUsuarioPesoInicial(pesoI float)
BEGIN
	select *
	from Usuario
	where	pesoInicial=pesoI;
  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS busquedaUsuarioDificultadFisica;
#Procedimiento de la clase UsuarioBusqueda
DELIMITER // 
CREATE PROCEDURE busquedaUsuarioDificultadFisica(IN difi varchar(30))
BEGIN
	select *
	from Usuario
	where	dificultadFisica like difi;
  
END //
DELIMITER ;

#-----------------------------------------------------------------------------------------------#
#Procedimientos COMIDAS

DROP PROCEDURE IF EXISTS grabarComida;
DELIMITER //
CREATE PROCEDURE grabarComida(In id int, in cal int, in nom varchar(20), in descr varchar(200), in platof boolean, in hor int)
BEGIN
	if id = 0 then
		Insert into Comida (calorias, nombre, descripcion, platoFuerte, hora) VALUES(cal, nom, descr, platof, hor);
		SELECT LAST_INSERT_ID();
	else
		update Comida set  calorias=calorias, nombre=nom, descripcion=descr, platoFuerte=platof, hora=hor
			where idComida = id;
	END IF;
  
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS cargarComida;
DELIMITER //
CREATE PROCEDURE cargarComida(in id int)
BEGIN
	select * from Comida
		where idComida= id;  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS eliminarComida;
DELIMITER //
CREATE PROCEDURE eliminarComida(in id int)
BEGIN
	delete from Comida
	where idComida=id;
  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS buscarComidaNombre;
DELIMITER //
CREATE PROCEDURE buscarComidaNombre(in nom varchar(20))
BEGIN
	select * from Comida
		where nombre like concat('%',nom,'%');  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS buscarComidaDescripcion;
DELIMITER //
CREATE PROCEDURE buscarComidaDescripcion(in descr varchar(20))
BEGIN
	select * from Comida
		where descripcion like concat('%',descr,'%');  
END //
DELIMITER ;

#---------------------------------------------------------------------------------------#
#Procedimientos HORACOMIDA

DROP PROCEDURE IF EXISTS grabarHoraComida;
DELIMITER //
CREATE PROCEDURE `grabarHoraComida` (IN id INT, IN horaC VARCHAR(20))
#SP para grabar en la tabla AreaDelCuerpo
BEGIN
	IF id = 0 THEN
		INSERT INTO HoraComida (hora) VALUES(horaC);
		SELECT LAST_INSERT_ID();
	ELSE
		UPDATE HoraComida SET hora=horaC WHERE idhoraComida=id;		
	END IF;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarHoraComidas;
DELIMITER //
CREATE PROCEDURE `cargarHoraComidas` ()
#SP para cargar desde la tabla AreaDelCuerpo
BEGIN
	SELECT * FROM HoraComida;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarHoraComida;
DELIMITER //
CREATE PROCEDURE `cargarHoraComida` (IN id INT)
#SP para cargar desde la tabla AreaDelCuerpo
BEGIN
	SELECT * FROM HoraComida WHERE idhoraComida=id;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS eliminarHoraComida;
DELIMITER //
CREATE PROCEDURE `eliminarHoraComida` (IN id INT)
#SP para eliminar un registro en la tabla AreaDelCuerpo
BEGIN
	DELETE FROM HoraComida WHERE idhoraComida=id;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS busquedaHoraComida;
DELIMITER //
CREATE PROCEDURE `busquedaHoraComida` (IN horaC varchar(20))
#SP para eliminar un registro en la tabla AreaDelCuerpo
BEGIN
	select * FROM HoraComida 
		WHERE hora like concat('%',horaC,'%');
END
//
DELIMITER ;


#------------------------------------------------------------------------------------------------#
#Procedimientos EJERCICIO
DROP PROCEDURE IF EXISTS grabarEjercicio;
DELIMITER //
CREATE PROCEDURE grabarEjercicio(in id int, in nom varchar(100), in descr varchar(200), in inte INT,
								in cal int(11))
BEGIN
	if id = 0 then
		Insert into Ejercicio (nombre, descripcion, intensidad, calorias) VALUES(nom, descr, inte, cal);
		SELECT LAST_INSERT_ID();
	else
		update Ejercicio
			set nombre=nom, descripcion=descr, intensidad=inte, calorias=cal
			where idEjercicio=id;
	END IF;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS eliminarEjercicio;
DELIMITER //
CREATE PROCEDURE eliminarEjercicio(id int)
BEGIN
	delete from Ejercicio
	where idEjercicio=id;
  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarEjercicio;
#Procedimiento de la clase EjercicioBUsqueda
DELIMITER // 
CREATE PROCEDURE cargarEjercicio(in id int)
BEGIN
	select *
	from Ejercicio
	where idEjercicio=id;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS busquedaEjercicioNombre;
#Procedimiento de la clase EjercicioBUsqueda
DELIMITER // 
CREATE PROCEDURE busquedaEjercicioNombre(in nombre varchar(100))
BEGIN
	select *
	from Ejercicio
	where nombre like concat('%', nombre, '%');
  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS busquedaEjercicioDescripcion;
#Procedimiento de la clase EjercicioBUsqueda
DELIMITER // 
CREATE PROCEDURE busquedaEjercicioDescripcion(in descripcion varchar(200))
BEGIN
	select *
	from Ejercicio
	where descripcion like concat('%', descripcion, '%');
  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS busquedaEjercicioADC;
#Procedimiento de la clase EjercicioBUsqueda
DELIMITER // 
CREATE PROCEDURE busquedaEjercicioADC(in areaDelCuerpo varchar(20))
BEGIN
	select e.*
	from Ejercicio e, AreaDelCuerpo a, EjercicioArea ea
	where 	ea.ejercicio=e.idEjercicio 
			and ea.area=a.idADC 
			and a.nombre like concat('%', areaDelCUerpo, '%');
  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS busquedaEjercicioIntensidad;
#Procedimiento de la clase EjercicioBUsqueda
DELIMITER // 
CREATE PROCEDURE busquedaEjercicioIntensidad(in inten varchar(8))
BEGIN
	select e.*
	from Ejercicio e, intensidadEjercicio i
	where e.intensidad=i.idIntensidad
		and i.intensidad like inten;
  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS reporteEjercicio_EnUso;
#Procedimiento de la clase EjercicioReportePanel
DELIMITER //
CREATE PROCEDURE reporteEjercicio_EnUso()
BEGIN
	select 	nombre, descripcion, intensidad, calorias
	from 	ejercicio
	where	ejercicio.idEjercicio in (	select 	e.idEjercicio 
										from 	Usuario u, RutinaUsuario ru, Rutina r, 
												EjercicioRutina er, Ejercicio e 
										where 	u.idUsuario=ru.usuario and ru.rutina=r.idRutina and 
												r.idRutina=er.rutina and er.ejercicio=e.idEjercicio and
												ejercicio.idEjercicio	);  
END //
DELIMITER ;


#------------------------------------------------------------------------------------------------#
#Procedimientos DIETA
DROP PROCEDURE IF EXISTS grabarDieta;
DELIMITER //
CREATE PROCEDURE grabarDieta(in id int, in nombreDieta varchar(20) )
BEGIN
	if id= 0 then
		Insert into Dieta (nombre) VALUES (nombreDieta);
		SELECT LAST_INSERT_ID();
	else
		update Dieta 
			set nombre=nombreDieta
			where idDieta=id;
  END IF;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS eliminarDieta;
DELIMITER //
CREATE PROCEDURE eliminarDieta(id int)
BEGIN
	delete from Dieta
	where idDieta=id;
  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS dietaCalculaCalorias;
DELIMITER $
CREATE PROCEDURE dietaCalculaCalorias(in id int)
BEGIN
	update Dieta 
		set ingestaCalorica = 
			(select sum(com.calorias) from ComidaDieta cd, Comida com where cd.comida = com.idComida and cd.dieta)
		where idDieta = id;
END $
DELIMITER ;

DROP PROCEDURE IF EXISTS dietaAgregaComida;
DELIMITER $
CREATE PROCEDURE dietaAgregaComida(in idD int, in idC int)
BEGIN
	if (select count(*) from ComidaDieta where comida = idC and dieta = idD) =0 then
		insert into ComidaDieta (comida, dieta) values (idC, idD);
	END IF;
END $
DELIMITER ;

DROP PROCEDURE IF EXISTS dietaBorrarComidas;
DELIMITER $
CREATE PROCEDURE dietaBorrarComidas(in idD int)
BEGIN
	delete from ComidaDieta where dieta = idD;
END $
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarDietaComidas;
DELIMITER $
CREATE PROCEDURE cargarDietaComidas(in idD int)
BEGIN
	select c.nombre, c.descripcion, c.calorias, c.PlatoFuerte, hc.hora from ComidaDieta cd, Comida c, HoraComida hc 
        where cd.dieta = idD and cd.comida = c.idComida and c.hora = hc.idHoraComida;
END $
DELIMITER ;

DROP PROCEDURE IF EXISTS busquedaDietaNombre;
DELIMITER //
CREATE PROCEDURE busquedaDietaNombre(in nom int)
BEGIN
	select * from Dieta
	where nombre like concat('%',nom,'%');
  
END //
DELIMITER ;


#--------------------------------------------------------------------------------------------------#
#Procedimientos RUTINA
DROP PROCEDURE IF EXISTS grabarRutina;
DELIMITER //
CREATE PROCEDURE grabarRutina(in id int, in nom varchar(50) , in descr varchar(200))
BEGIN
	if id = 0 then
		Insert into Rutina (nombre, descripcion) VALUES(nom, descr);
	else 
		update Rutina
			set nombre=nom, descripcion=descr
			where idRutina=id;
	END IF;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarRutina;
DELIMITER //
CREATE PROCEDURE cargarRutina(in id int)
BEGIN
	select * from Rutina
		where idRutina = id;
  
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS eliminarRutina;
DELIMITER //
CREATE PROCEDURE eliminarRutina(in id int)
BEGIN
	delete from Rutina
	where idRutina=id;
  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS busquedaRutinaNombre;
DELIMITER //
CREATE PROCEDURE busquedaRutinaNombre(in nom int)
BEGIN
	select * from Rutina
	where nombre like concat('%',nom,'%');
  
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS busquedaRutinaDescripcion;
DELIMITER //
CREATE PROCEDURE busquedaRutinaDescripcion(in descr int)
BEGIN
	select * from Rutina
	where descripcion like concat('%',descr,'%');
  
END //
DELIMITER ;



#--------------------------------------------------------------------------------------------------#
#Procedimientos RUTINAUSUARIO
DROP PROCEDURE IF EXISTS grabarRutinaUsuario;
DELIMITER //
CREATE PROCEDURE grabarRutinaUsuario(in idR int, in idU int, in fechaI date, in fechaF date)
BEGIN
	insert into RutinaUsuario (rutina, usuario, fechaInicio, fechaFin) values (idR, idU, fechaI, fechaF);
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarRutinasUsuario;
DELIMITER //
CREATE PROCEDURE cargarRutinasUsuario(in idU int)
BEGIN
	select rutina from RutinaUsuario where usuario = idU;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS borrarRutinasUsuario;
DELIMITER //
CREATE PROCEDURE borrarRutinasUsuario(in idU int)
BEGIN
	delete from RutinaUsuario where usuario = idU;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS borrarRutinaUsuario;
DELIMITER //
CREATE PROCEDURE borrarRutinaUsuario(in idU int, in idR int)
BEGIN
	delete from RutinaUsuario where usuario = idU and rutina = idR;
END //
DELIMITER ;


#--------------------------------------------------------------------------------------------------#
#Procedimientos EJERCICIORUTINA
DROP PROCEDURE IF EXISTS grabarEjercicioRutina;
DELIMITER //
CREATE PROCEDURE grabarEjercicioRutina(in idE int, in idR int)
BEGIN
	if (select count(*) from EjercicioRutina where ejercicio = idE and rutina = idR) = 0 then
		insert into EjercicioRutina (ejercicio, rutina) values (idE, idR);
	END IF;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS borrarEjerciciosRutina;
DELIMITER //
CREATE PROCEDURE borrarEjerciciosRutina(in idR int)
BEGIN
	delete from EjercicioRutina where rutina = idR;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarEjerciciosRutina;
DELIMITER //
CREATE PROCEDURE cargarEjerciciosRutina(in idR int)
BEGIN
	select e.* from EjercicioRutina er, Ejercicio e where er.rutina = idR and er.ejercicio = e.idEjercicio;
END //
DELIMITER ;

#--------------------------------------------------------------------------------------------------#
#Procedimientos EJERCICIOAREA
DROP PROCEDURE IF EXISTS grabarEjercicioArea;
DELIMITER //
CREATE PROCEDURE grabarEjercicioArea(in idE int, in idADC int)
BEGIN
	if (select count(*) from EjercicioArea where ejercicio = idE and area = idADC) = 0 then
		insert into EjercicioArea (ejercicio, area) values (idE, idADC);
	END IF;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS borrarEjercicioAreas;
DELIMITER //
CREATE PROCEDURE borrarEjercicioAreas(in idE int)
BEGIN
	delete from EjercicioArea where ejercicio = idE;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarEjercicioArea;
DELIMITER //
CREATE PROCEDURE cargarEjercicioArea(in idE int)
BEGIN
	select area from EjercicioArea ea, AreaDelCuerpo adc where ea.ejercicio = idE and ea.area = adc.idadc;
END //
DELIMITER ;

#--------------------------------------------------------------------------------------------------#
#Procedimientos FACTURA
DROP PROCEDURE IF EXISTS grabarFactura;
DELIMITER //
CREATE PROCEDURE grabarFactura(in idUsuario int)
BEGIN
	set @dias = (select t.diasVigencia from Usuario u, TipoMembresia t where u.tipoMembresia = t.idTipoMembresia and u.idUsuario=idU);
	set @montoF = (select t.costoMembresia from Usuario u, TipoMembresia t where u.tipoMembresia = t.idTipoMembresia and u.idUsuario=idU);
	Insert into Factura (fechaEmision, fecchaCredito, monto, usuario) VALUES (curdate(), adddate(curdate(), @dias), @montoF, idUsuario);
	call usuarioIncrementarFechaFin(usuario);
	select last_insert_id();
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS anularFactura;
DELIMITER //
CREATE PROCEDURE anularFactura(in id int)
BEGIN
	update Factura 
		set valido=false 
		where idFactura = id;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarFactura;
DELIMITER //
CREATE PROCEDURE cargarFactura(in id int)
BEGIN
	select * from Factura where idFactura = id;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS buscaFacturaUsuario;
DELIMITER //
CREATE PROCEDURE buscaFacturaUsuario(in idU int)
BEGIN
	select * from Factura where usuario = idU;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS usuarioIncrementarFechaFin;
DELIMITER //
CREATE PROCEDURE usuarioIncrementarFechaFin(in idU int)
BEGIN
	set @dias = (select t.diasVigencia from Usuario u, TipoMembresia t where u.tipoMembresia = t.idTipoMembresia and u.idUsuario=idU);
	if (select fechaFinMembresia from Usuario where idUsuario=idU)>curdate() then
		update Usuario
			set fechaFinMembresia=adddate(fechaFinMembresia, @dias)
			where idUsuario=idU;
	else
		update Usuario
			set fechaFinMembresia=adddate(curdate(), @dias)
			where idUsuario=idU;
	end if;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS usuarioDisminuirFechaFin;
DELIMITER //
CREATE PROCEDURE usuarioDisminuirFechaFin(in idU int)
BEGIN
	set @dias = (select t.diasVigencia from Usuario u, TipoMembresia t where u.tipoMembresia = t.idTipoMembresia and u.idUsuario=idU);
	if (select fechaFinMembresia from Usuario where idUsuario=idU)>curdate() then
		update Usuario
			set fechaFinMembresia=subdate(fechaFinMembresia, @dias)
			where idUsuario=idU;
	else
		update Usuario
			set fechaFinMembresia=subdate(curdate(), @dias)
			where idUsuario=idU;
	end if;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS facturaCalculaSaldo;
DELIMITER //
CREATE PROCEDURE facturaCalculaSaldo (IN idF INT)
BEGIN
	UPDATE factura
		set saldo = monto - (select sum(p.monto) from Pago p where p.factura=idF and p.valido)
		where idFactura=idF;
END //
DELIMITER ;


#Procedimientos TIPOMEMBRESIA

DROP PROCEDURE IF EXISTS grabarTipoMembresia;
DELIMITER //
CREATE PROCEDURE `grabarTipoMembresia` (IN id INT, IN nombre VARCHAR(20), in dias int, in costo decimal)
#SP para grabar en la tabla TipoMembresia
BEGIN
	IF id = 0 THEN
		INSERT INTO TipoMembresia (nombreTipoMembresia, diasVigencia, costoMembresia) VALUES(nombre, dias, costo);
		SELECT LAST_INSERT_ID();
	ELSE
		UPDATE TipoMembresia 
			SET nombreTipoMembresia=nombre, diasVigencia=dias, costoMembresia=costo 
			WHERE idTipoMembresia=id;		
	END IF;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarTiposMembresia;
DELIMITER //
CREATE PROCEDURE `cargarTiposMembresia` ()
#SP para cargar desde la tabla TipoMembresia
BEGIN
	SELECT * FROM TipoMembresia;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarTipoMembresia;
DELIMITER //
CREATE PROCEDURE `cargarTipoMembresia` (IN id INT)
#SP para eliminar un registro en la tabla TipoMembresia
BEGIN
	SELECT * FROM TipoMembresia WHERE idTipoMembresia=id;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS buscarTipoMembresiaNombre;
DELIMITER //
CREATE PROCEDURE `buscarTipoMembresiaNombre` (IN nom VARCHAR(20))
#SP para cargar desde la tabla TipoMembresia
BEGIN
	SELECT * FROM TipoMembresia where nombreTipoMembresia like concat('%',nom,'%');
END
//
DELIMITER ;


DROP PROCEDURE IF EXISTS eliminarTipoMembresia;
DELIMITER //
CREATE PROCEDURE `eliminarTipoMembresia` (IN id INT)
#SP para eliminar un registro en la tabla TipoMembresia
BEGIN
	DELETE FROM TipoMembresia WHERE idTipoMembresia=id;
END
//
DELIMITER ;

#--------------------------------------------------------------------------------------------------#
#Procedimientos PAGO
DROP PROCEDURE IF EXISTS grabarPago;
DELIMITER //
CREATE PROCEDURE grabarPago(in idFactura int, in descr varchar(50), in montoP decimal)
BEGIN
	Insert into Pago (fecha, Factura, descripcion , monto) 
		VALUES (curdate(), idFactura, descr, montoP);
	select last_insert_id();
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS anularPago;
DELIMITER //
CREATE PROCEDURE anularPago(in id int)
BEGIN
	update Pago 
		set valido=false
		where idPago= id;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarPago;
DELIMITER //
CREATE PROCEDURE cargarPago(in id int)
BEGIN
	select * from Pago where idPago = id;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS buscaPagosUsuario;
DELIMITER //
CREATE PROCEDURE buscaPagosUsuario(in idU int)
BEGIN
	select p.* from Pago p, Factura f
	where p.factura = f.idFactura and  f.usuario = idU;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS buscaPagosUsuarioNombre;
DELIMITER //
CREATE PROCEDURE buscaPagosUsuarioNombre(in nombreU varchar(50))
BEGIN
	select p.*
		from Pago p, Usuario u, Factura f
		where p.factura = f.idFactura and f.usuario = u.idUsuario and u.nombre like concat('%', nombreU, '%');
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS buscaPagosFecha;
DELIMITER //
CREATE PROCEDURE buscaPagosFecha(in fechaP date)
BEGIN
	select *
		from Pago p
		where p.fecha = fechaP;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS cargaPagosFactura;
DELIMITER //
CREATE PROCEDURE cargaPagosFactura(in idF int)
BEGIN
	select * from Pago where factura = idF;
END //
DELIMITER ;

#--------------------------------------------------------------------------------------------------#
#Procedimientos USUARIODIETA
DROP PROCEDURE IF EXISTS grabarUsuarioDieta;
DELIMITER //
CREATE PROCEDURE grabarUsuarioDieta(in idD int, in idU int)
BEGIN
	if (select count(*) from UsuarioDieta where dieta = idD and  usuario = idU) = 0 then
		insert into UsuarioDieta (dieta, usuario) values (idD, idU);
	END IF;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS cargarUsuarioDieta;
DELIMITER //
CREATE PROCEDURE cargarUsuarioDieta(in idU int)
BEGIN
	select dieta from UsuarioDieta where usuario = idU;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS borrarUsuarioDietas;
DELIMITER //
CREATE PROCEDURE borrarUsuarioDietas(in idU int)
BEGIN
	delete from UsuarioDieta where usuario = idU;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS borrarUsuarioDieta;
DELIMITER //
CREATE PROCEDURE borrarUsuarioDieta(in idD int, in idU int)
BEGIN
	delete from UsuarioDieta where usuario = idU and dieta = idD;
END //
DELIMITER ;


#------------------------------------------------------------------

DROP PROCEDURE IF EXISTS mostrarPorcentajeEjercicios;
DELIMITER //
CREATE PROCEDURE mostrarPorcentajeEjercicios()
BEGIN
	SELECT (COUNT(u.nombre)/20)*100 as Porcentaje, e.nombre
	FROM Ejercicio e, Usuario u , Rutina r, RutinaUsuario ru, EjercicioRutina re 
	WHERE u.idUsuario = ru.usuario AND r.idRutina = ru.rutina AND e.idEjercicio = re.ejercicio AND r.idRutina = re.rutina
	GROUP BY e.nombre
	ORDER BY Porcentaje DESC;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS mostrarEjerciciosMasUsados;
DELIMITER //
CREATE PROCEDURE mostrarEjerciciosMasUsados()
BEGIN

	SELECT e.nombre, e.descripcion, e.intensidad
	FROM Ejercicio e
	WHERE e.nombre IN
					(	SELECT e.nombre
						FROM Ejercicio e, Usuario u , Rutina r, RutinaUsuario ru, EjercicioRutina re 
						WHERE u.idUsuario = ru.usuario AND r.idRutina = ru.rutina AND e.idEjercicio = re.ejercicio AND r.idRutina = re.rutina
						GROUP BY e.nombre	)
	ORDER BY e.nombre DESC;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS mostrarEjerciciosMenosUsados;
DELIMITER //
CREATE PROCEDURE mostrarEjerciciosMenosUsados()
BEGIN
	
	SELECT e.nombre, e.descripcion, e.intensidad
	FROM Ejercicio e
	WHERE e.nombre NOT IN
					(	SELECT e.nombre
						FROM Ejercicio e, Usuario u , Rutina r, RutinaUsuario ru, EjercicioRutina re 
						WHERE u.idUsuario = ru.usuario AND r.idRutina = ru.rutina AND e.idEjercicio = re.ejercicio AND r.idRutina = re.rutina
						GROUP BY e.nombre	)
	ORDER BY e.nombre DESC;

END //
DELIMITER ;

