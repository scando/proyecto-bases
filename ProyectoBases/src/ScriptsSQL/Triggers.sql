use BaseASIVISA;

drop trigger if exists trigCaloriasDieta1;
delimiter $$
create trigger trigCaloriasDieta1 after insert on ComidaDieta
	for each row call dietaCalculaCalorias(new.comida);
$$
delimiter ;

drop trigger if exists trigCaloriasDieta2;
delimiter $$
create trigger trigCaloriasDieta2 after delete on ComidaDieta
	for each row call dietaCalculaCalorias(old.comida);
$$
delimiter ;

drop trigger if exists trigCaloriasDieta3;
delimiter $$
create trigger trigCaloriasDieta3 after update on ComidaDieta
	for each row call dietaCalculaCalorias(new.comida);
$$
delimiter ;

drop trigger if exists trigUsuarioDieta;
delimiter $$
create trigger trigUsuarioDieta before insert on UsuarioDieta
	for each row begin
		if (select count(*) from UsuarioDieta ud 
				where ud.usuario = new.usuario and (ud.fechaInicio between new.fechaInicio and new.fechafin
						or ud.fechaInicio between new.fechaInicio and new.fechafin))>0 then
			signal sqlstate '45000' set message_text= 'Error al agregar dieta. Dieta ya asignada al intervalo.';
		end if;
	end; $$
delimiter ;

drop trigger if exists trigUsuarioRutina;
delimiter $$
create trigger trigUsuarioRutina before insert on RutinaUsuario
	for each row begin
		if (select count(*) from RutinaUsuario ru 
				where ru.usuario = new.usuario and ru.rutina = new.rutina and (ru.fechaInicio between new.fechaInicio and new.fechafin
						or ru.fechaInicio between new.fechaInicio and new.fechafin))>0 then
			signal sqlstate '45000' set message_text= 'Error al agregar Rutina. Rutina repetida durante el intervalo.';
		end if;
	end;
$$
delimiter ;

drop trigger if exists trigSaldoFactura1;
delimiter $$
create trigger trigSaldoFactura after insert on Pago
	for each row call facturaCalculaSaldo(new.factura);
$$
delimiter ;
