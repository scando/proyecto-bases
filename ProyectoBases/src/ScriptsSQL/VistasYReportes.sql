
drop view if exists vistaUsuarioDeudores;
create view vistaUsuarioDeudores as
	select u.nombre, u.email, sum(saldo) deuda
		from Usuario u, Factura f 
		where u.idUsuario=f.usuario and f.valido
		group by usuario;

drop view if exists vistaUsuarioActivo;
create view vistaUsuarioActivo as
	select u.nombre, u.email, u.fechaInicioMembresia, u.fechaFinMembresia 
		from Usuario u
		where u.fechaFinMembresia > current_date;

drop view if exists vistaUsuarioInactivo;
create view vistaUsuarioInactivo as
	select u.nombre, u.email, u.fechaInicioMembresia, u.fechaFinMembresia 
		from Usuario u
		where u.fechaFinMembresia <= current_date;

drop view if exists vistaUsuarioPagosTotales;
create view vistaUsuarioPagosTotales as
	select u.nombre, sum(p.monto) PagosTotales
		from Usuario u, Pago p, Factura f
		where p.factura=f.idFactura and f.usuario=u.idUsuario and f.valido and p.valido
		group by u.idUsuario;

drop view if exists vistaUsuarioPagosAnio;
create view vistaUsuarioPagosAnio as
	select u.nombre, sum(p.monto) PagosAnio
		from Usuario u, Pago p, Factura f
		where p.factura=f.idFactura and f.usuario=u.idUsuario and f.valido and p.valido 
			and datediff(p.fecha,curdate()) between 0 and 365
		group by u.idUsuario;

drop view if exists vistaUsuarioPagosMes;
create view vistaUsuarioPagosMes as
	select u.nombre, sum(p.monto) PagosAnio
		from Usuario u, Pago p, Factura f
		where p.factura=f.idFactura and f.usuario=u.idUsuario and f.valido and p.valido 
			and datediff(p.fecha,curdate()) between 0 and 30
		group by u.idUsuario;

drop view if exists vistaUsuarioPorAgotar;
create view vistaUsuarioPorAgotar as
	select u.nombre, u.email, u.fechaFinMembresia 
		from Usuario u
		where datediff(u.fechaFinMembresia,curdate()) <= 15 and u.fechaFinMembresia>curdate();

DROP PROCEDURE IF EXISTS deudoresReport;
DELIMITER //
CREATE PROCEDURE deudoresReport ()
BEGIN
	SELECT * FROM vistaUsuarioDeudores;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS usuarioActivosReport;
DELIMITER //
CREATE PROCEDURE usuarioActivosReport ()
BEGIN
	SELECT * FROM vistaUsuarioActivo;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS pagosReport;
DELIMITER //
CREATE PROCEDURE pagosReport ()
BEGIN
	SELECT * FROM ;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS Report;
DELIMITER //
CREATE PROCEDURE Report ()
BEGIN
	SELECT * FROM ;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS Report;
DELIMITER //
CREATE PROCEDURE Report ()
BEGIN
	SELECT * FROM ;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS Report;
DELIMITER //
CREATE PROCEDURE Report ()
BEGIN
	SELECT * FROM ;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS Report;
DELIMITER //
CREATE PROCEDURE Report ()
BEGIN
	SELECT * FROM ;
END //
DELIMITER ;
