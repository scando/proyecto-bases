
INSERT INTO IntensidadEjercicio (intensidad) VALUES('ALTA');
INSERT INTO IntensidadEjercicio (intensidad) VALUES('MEDIA-ALTA');
INSERT INTO IntensidadEjercicio (intensidad) VALUES('MEDIA');
INSERT INTO IntensidadEjercicio (intensidad) VALUES('MEDIA-BAJA');
INSERT INTO IntensidadEjercicio (intensidad) VALUES('BAJA');

INSERT INTO HoraComida(Hora)values('DESAYUNO');
INSERT INTO HoraComida(Hora)values('LUNCH');
INSERT INTO HoraComida(Hora)values('ALMUERZO');
INSERT INTO HoraComida(Hora)values('MEAL');
INSERT INTO HoraComida(Hora)values('MERIENDA');


INSERT INTO TipoMembresia(nombretipomembresia,diasvigencia,costomembresia)values
('MENSUAL',30,'100.0'),
('SEMANAL',7,'30.0'),
('DIARIO',1,'5.0');

INSERT INTO AreaDelCuerpo(nombre)values
('CUELLO'),
('HOMBROS'),
('ESPALDA'),
('BRAZOS'),
('TRICEPS'),
('PECHO'),
('BICEPS'),
('ANTEBRAZOS'),
('ABOMINALES'),
('ABDOMINALES'),
('MUSLOS'),
('CLUTEOS'),
('FEMORALES'),
('GEMELOS');

/**
URL: http://www.taringa.net/post/deportes/2111294/Todos-los-Musculos-y-Todos-sus-Ejercicios-Para-el-gym.html
**/

INSERT INTO Ejercicio (nombre,descripcion,intensidad,calorias) values
('ENCOGIMIENTO ABDOMINAL','ACOSTADO BOCA ARRIBA, MANOS DETRAS DE LA CABEZA, MUSLOS EN LA VERTICAL,RODILLAS FLEXIONADAS',2,100),
('ELEVACION DEL TRONCO EN EL SUELO','ACOSTADO BOCA ARRIBA, RODILLAS FLEXIONADAS, PIES EN EL SUELO, MANOS DETRAS DE LA CABEZA',3,75),
('ROTACION DEL TRONCO CON BASTON','DE PIE, PIERNAS SEPARADAS, UN BASTON AL NIVEL DE LOS TRAPECIOS, MANOS APOLLADAS SIN APRETARLAS',5,25),
('ELEVACION DEL TRONCO EN BANCO INCLINADO','SENTADOS EN EL BANCO, PIES BAJO LOS COJINES, MANO DETRAS DE LA NUCAINSPIRAR E INCLINAR EL TORSO SIN SOBREASAR LOS 20 GRADOS',4,50),
('ELEVACIONES DE TRONCO EN SUSPENCIONDE BANCO ESPECIFICO','PIES FIJADOS EN LOS COJINES, TRONCO EN EL VACIO, MANOS DETRAS DE LA CABEZA',1,125),
('ENCOGIMIENTO ABDOMINAL CON POLEA ALTA','DE RODILLAS BARRA DETRAS DE LA NUCA ',4,75),
('ELEVACIONES DE PIERNAS CON PLANCA INCLINADA','ESTIRADO SOBRE LA PLANCHA INCLINADA, ELEVAR LAS PIERNAS HASTA LA HORIZONTAL,INTENTAR TOCAR LA CABEZA CON LAS RODILLAS',1,125),
('ELEVACION DE RODILLAS EN PARALELAS','APOYADO SOBRE LOS CODOS, ESPALDA FIJA, ELEVA LAS RODILLAS HACIA EL PECHO FLEXIONANDO LA ESPALDA',2,100),
('PRESS CON MANCUERDA','SENTADO EN UN BANCO, BRAZOS EXTENDIDO EN EL VERTICAL, MANOS FRENTE A FRENTE COGIENDO LAS MANCUERNA',5,25),
('ELEVACION DE PIERNAS, SUSPENDIDO EN BARRA FIJA','ELEVAR LAS RODILLAS LO MAS ALTO POSIBLE PROCURANDO ACERCAR LA PUBIS AL EXTERNON, FELXIONANDO LA COLUMNA',2,100),
('FLEXION LATERAL DEL TRONCO CON MANCUERNA','DE PIE LAS PIERNAS LIGERAMENTE SEPARADAS, UN BRAXO DETRAS DE LA CABEZA, UNA MANCUERNA EN UNA MANO',1,25),
('FLEXION LATERAL DEL TRONCO EN BANCO','LA CADERA APOYADA EN EL BANCO, TRONCO EN EL VACIO, MANOS DETRAS DE LA CABEZA O SOBRE EL PECHO,PIES FIJOS BAJO LOS COJINES',3,75),
('EL TWIST','DE PIE SOBRE LA PLACA GIRATORIA, MANOS COLOCADAS SOBRE LOS AGARRES',5,25),
('CURL DE BICEPS CON BARRA','DE PIE CON LAS PIERNAS LIGERAMENTE SEPARADAS, BRAZOS EXTENDIDOS, FLEXION DE ANTEBRAZOS ',4,75),
('CURL DE BICEPS EN EL BANCO SCOTT','SENTADO O DE PIE APOYADOS EN EL BANCO <LARRY SCOTT>, FLEXION DE ANTEBRAZOS',2,100),
('CURL DE BICEPS ALTERNO <TIPO MARTILLO>','DE PIE O SENTADO, CON UNA MANCUERNA EN CADA MANO COGIDA EN SEMI PRONACION UNA FLEXION SIMULTANEA ALTERNATIVAMENTE',5,25),
('CURL DE BICEPS ALTERNOS CON SUPINACION','SENTADO, UNA MACUERNA EN CADA MANO COGIDAS EN SEMIPRONACION, FLEXIONAR LOS ANTEBRAZOS HACIA DELANTE CON ROTACION DE MUNECA ANTES DE ALCANZAR LA HORIZONTAL',4,50),
('CURL DE BICEPS CONCENTRADO CON APOYO EN EL MUSLO','CON UNA MANCUERNA COGIDA Y EL CODO APOYADO EN LA CARA INTERNA DEL MUSLO FLEXION DE ANTEBRAZO ESPIRAR AL FINAL DEL ESFUERZO',5,25),
('CURL DE BICEPS CON BARRA Y AGARRE EN PRONACION','DE PIE, PIERNAS LIGERAMENTE SEPARADAS, BRAZOS EXTENDIDOS, MANOS EN PROPINACION ',3,75),
('CURL DE BICEPS CON POLEA','DE PIE , DE LA CARA  AL APARATO, MANGO COGIDO CON SUPINACION ,EFECTUAR UNA FLEXION DE LOS ANTEBRAZOS',5,25);

/**
 URL: http://www.fitnessbliss.com/es/rutinas-de-entrenamiento/
**/

INSERT INTO Rutina (Nombre,Descripcion) values
('ABDOMINALES CON MANCUERNAAS','EJERCICIOS PARA ABDOMINALES CON AYUDA DE MANCUERNAS FACILES DE REALIZAR'),
('ESPALDA CON MANCUERNAS','EJERCICIOS PARA SECCION ESPALDA, MANCUERNAS Y PESO MUERTO'),
('PECHO CON MANCUERNAS','RUTINA PARA SECCION PECHO, MANCUERNAS REQUERIDAS'),
('HOMBROS CON MANCUERNAS','HOMBROS, MANCUERNAS Y POLEA'),
('PIERNAS CON MANCUERDAS','EJERCICIOS PARA FORTALECER PIERNAS CON MANCUERNAS Y PESO MUERTO'),
('BRAZOS CON MANCUERNAS','EJERCICIOS PARA BRAZOS CON MANCUERNAS, DE PIE O SENTADOS'),
('ABDOMINALES CON BARRA','RUTINA PARA QUEMAR ABDOMINALES NIVEL MEDIO'),
('ESPALDA CON BARRA','RUTINA PARA ESPALDA CON AYUDA DE BARRA'),
('PECHO CON BARRA','EJERCICIOS PARA PECHO CON BARRA Y MAQUINA'),
('HOMBROS CON BARRA','RUTINA DE HOMBROS CON BARRA Y MAQUINA'),
('PIERNAS CON BARRA','RUTINA CON BARRA PARA PIERNAS'),
('BRAZOS CON BARRA','RUTINA CON BARRA PARA BRAZOS'),
('ABDOMINALES CON PELOTA','EJERCICIOS CON PELOTA PARA ABDOMINALES'),
('ESPALDA CON PELOTA','RUTINA DE EJERCICIOS PARA ESPALDA CON AYUDA DE PELOTA'),
('PECHO CON PELOTA','EJERCICIOS CON PELOTA PARA PECHO'),
('HOMBROS CON PELOTA','RUTINA DE HOMBROS CON PELOTA'),
('GUIMNASIO PARA PECHO','MAQUINAS Y PESAS PARA PECHO'),
('GIMNASIO PARA ESPALDA','MAQUINAS Y PESAS PARA ESPALDA'),
('GIMNASIO PARA BRAZOS','PESAS PARA BRAZOS'),
('GIMNASIO PARA ABDOMINALES','SILLAS, MAQUINAS Y MANCUERNAS PARA ABDOMINALES');


INSERT INTO Comida(nombre,descripcion,calorias,hora,platofuerte) values
('POLLO A LA PLANCHA','100 GRAMOS',98,3,1),
('POOLO COCIDO','100 GRAMOS',100,3,1),
('POLLO ASADO','100 GRAMOS',110,3,1),
('HIGADO DE POLLO','100 GRAMOS',124,3,1),
('FILETE DE TERNERA','100 GRAMOS',180,3,1),
('CORDERO ASADO','100 GRAMOS',194,3,1),
('PERDIZ ASADA','100 GRAMOS',206,3,1),
('HIGADO DE BUEY','100 GRAMOS',210,3,1),
('TERNERA ASADA','100 GRAMOS',231,3,1),
('BUEY GUISADO','100 GRAMOS',235,3,1),
('FILETE DE CERDO','100 GRAMOS',240,3,1),
('HIGADO DE TERNERA','100 GRAMOS',256,3,1),
('TERNERA GUISADA','100 GRAMOS',256,3,1),
('HIGADO DE CERDO','100 GRAMOS',264,3,1),
('BUEY ASADO','100 GRAMOS',288,3,1),
('PATO ASADO','100 GRAMOS',320,3,1),
('CHULETA DE CERDO','100 GRAMOS',336,3,1),
('CHULETA DE CORDERO','100 GRAMOS',356,3,1),
('CABRITO ASADO','100 GRAMOS',357,3,1),
('LOMO DE CERDO','100 GRAMOS',362,3,1),
('JAMON DE CERDO','100 GRAMOS',393,3,1),
('TOCINO AHUMANO','100 GRAMOS',665,3,1),
('TOCINO FRITO','100 GRAMOS',870,3,1);

/**
('POLLO A LA PLANCHA','100 GRAMOS',98,3,1),
('POLLO A LA PLANCHA','100 GRAMOS',98,3,1),
('POLLO A LA PLANCHA','100 GRAMOS',98,3,1),
('POLLO A LA PLANCHA','100 GRAMOS',98,3,1),
('POLLO A LA PLANCHA','100 GRAMOS',98,3,1);
**/
INSERT INTO Dieta(nombre)VALUES

('DIETA DE LA MANZANA'),
('DIETA LEZARTE'),
('DIETA DE CARBOHIDRATOS'),
('DIETA SPRING'),
('DIETA ALL CAN YOU EAT'),
('DIETA DE LA GUAYABA'),
('DIETA GROUSO'),
('DIETA PONCE ARTURO'),
('DIET STRAKS'),
('DIETA PAPAYA'),
('DIETA PASTA'),
('DIETA DEL Dr DUKAN'),
('DIETA DE LA SOPA DE TOMATE'),
('DIETA CONTRA CELULITIS'),
('DIETA DE LA CLINICA MAYO'),
('DIETA MEDITERRANEA'),
('DIETA MONTIGNAG'),
('DIETA ATKINS'),
('DIETA DEL PLATANO'),
('DIETA SCARDALE');


INSERT INTO Usuario(cedula,nombre,username,contrasena,email,residencia,pais,ciudad,pesoinicial,pesoactual,pesometa,estatura,tipomembresia,fechaInicioMembresia,fechaFinMembresia,perfilpublico)values
('0948754650','kevin cando','kcando','kcando','kcando@hotmail.com','urdesa central av 1ra y calle 3ra #3002',' Ecuador','Guayaquil',180,190,168,180,1,'2015-02-12','2015-03-12',0),
('1625264815','juan alvarado','jalvarado','jalvarado','jalvarado@hotmail.com','ciudadela guayacanes mz 4 villa 8',' Ecuador','Guayaquil',170,160,168,172,1,'2015-02-10','2015-03-10',0),
('1308303047','sergey baidal','sbaidal','sbaidal','sbaidal@hotmail.com','samanes av delta #1209 y oriente ',' Ecuador','Quito',270,260,200,180,1,'2015-02-01','2015-03-01',0),
('1715487884','sucre garces','sgarces','sgarces','sgarces@hotmail.com','Sauces 4 mz 3 villa 21',' Ecuador','guayaquil',200,195,166,158,2,'2015-02-26','2015-03-05',1),
('0875421678','elias triana','etriana','etriana','etriana@hotmail.com','cdla via al sol villa 203',' Ecuador','guayaquil',177,168,156,162,1,'2015-02-11','2015-03-11',1),
('1201127576','maria macias','mmacias','mmacias','mmacias@hotmail.com','cuenca 232 y quito',' Ecuador','guayaquil',220,200,198,169,2,'2015-02-22','2015-02-26',1),
('1706778675','jose baidal','jbaidal','jbaidal','jose_baidal@hotmail.com','leonidas plaza 919 capitan najera',' Ecuador','guayaquil',170,160,154,168,1,'2015-02-09','2015-03-09',1),
('0425784125','ariana torres','atorres','atorres','atorres@yahoo.com','peru 4444 y lima',' Ecuador','Taday',230,230,220,215,1,'2015-02-09','2015-03-09',1),
('1308303039','eduardo galan','egalan','egalan','egalan@hotmail.com','1av  12th street 1300','Bolibia','la paz',199,190,170,169,1,'2015-02-05','2015-03-05',0),
('0848741036','eli cedeno','ecedeno','ecedeno','ecedeno@hotmail.com','portal al sol mz5 villa 2',' Ecuador','Guayaquil',150,140,130,150,1,'2015-02-15','2015-03-15',1),
('3356847929','maria figueroa','mfigueroa','mfigueroa','mfigueroa@yahoo.com','ciudadela guangala ',' Ecuador','guayaquil',180,190,168,180,1,'2015-02-12','2015-03-12',1),
('1212457884','jeny toledo','jtoledo','jtoledo','jtoledo@hotmail.com','45 NE y 32 street','Estados Unidos','Atlanta',180,186,168,170,2,'2015-02-05','2015-02-12',0),
('0995511559','andrea negrete','anegrete','anegrete','anegrete@hotmail.com','urdesa central costanera y calle 3ra #5682',' Ecuador','Guayaquil',166,177,155,178,2,'2015-02-25','2015-03-04',0),
('0857424219','francia lopez','flopez','flopez','flopez@hotmail.com','samboromdon av 1ra y calle 3ra #3002',' Ecuador','Samboromdon',150,150,140,160,1,'2015-02-10','2015-03-10',1),
('0785474102','miguel salazar','msalazar','msalazar','msalazar@hotmail.com','17 y la "D" 8574',' Ecuador','Guayaquil',190,180,170,178,1,'2015-02-08','2015-03-08',0),
('1725522562','raul aroca','raroca','raroca','raroca@hotmail.com','cdla esmeraldas mz 5 villa 21',' Ecuador','cuenca',155,145,133,155,2,'2015-02-19','2015-03-19',1),
('0668423565','pedro perez','pperez','pperez','pperez@hotmail.com','cedros mz 2 villa 12',' Ecuador','Ambato',180,170,168,180,1,'2015-02-01','2015-03-01',0),
('0758984824','paco flores','pflores','pfrores','pflores@hotmail.com','malecon y 1era',' Ecuador','Salinas',169,158,145,154,1,'2015-02-07','2015-03-07',1),
('1154842203','francisco ramos','framos','framos','framos@hotmail.com','argentina 3443 ambato',' Ecuador','santo domingo',160,140,138,148,2,'2015-02-11','2015-02-18',0),
('0948754651','kerwin cross','kcross','kcross','kcross@hotmail.com','urdesa central av 2ra y calle 5ra #4872',' Ecuador','guayaquil',175,175,168,170,1,'2015-03-01','2015-03-02',0);

insert into EjercicioArea (ejercicio,area)values
(1,9),
(2,3),
(3,6),
(4,2),
(5,11),
(6,4),
(7,9),
(8,3),
(9,6),
(10,2),
(11,11),
(12,4),
(13,9),
(14,3),
(15,6),
(16,11),
(17,6),
(18,3),
(19,4),
(20,9);

insert into ComidaDieta (dieta,comida)values
(1,19),
(2,13),
(3,16),
(4,12),
(5,11),
(6,14),
(7,9),
(8,3),
(9,6),
(10,2),
(11,11),
(12,4),
(13,1),
(14,5),
(15,6),
(16,11),
(17,6),
(18,8),
(19,4),
(20,15);

insert into EjercicioRutina (rutina,ejercicio)values
(1,9),
(2,3),
(3,6),
(4,2),
(5,11),
(6,4),
(7,9),
(8,3),
(9,6),
(10,2),
(11,11),
(12,4),
(13,9),
(14,3),
(15,6),
(16,11),
(17,6),
(18,3),
(19,4),
(20,9);

insert into RutinaUsuario (rutina,usuario,fechainicio,fechafin) values
(1,21,curdate(),adddate(curdate(),5)),
(2,23,curdate(),adddate(curdate(),7)),
(3,26,curdate(),adddate(curdate(),6)),
(4,24,curdate(),adddate(curdate(),3)),
(5,21,curdate(),adddate(curdate(),2)),
(6,34,curdate(),adddate(curdate(),1)),
(7,39,curdate(),adddate(curdate(),5)),
(8,33,curdate(),adddate(curdate(),3)),
(9,26,curdate(),adddate(curdate(),4)),
(10,32,curdate(),adddate(curdate(),10)),
(11,21,curdate(),adddate(curdate(),11)),
(12,34,curdate(),adddate(curdate(),3)),
(13,39,curdate(),adddate(curdate(),7)),
(14,32,curdate(),adddate(curdate(),15)),
(15,36,curdate(),adddate(curdate(),5)),
(16,31,curdate(),adddate(curdate(),7)),
(17,26,curdate(),adddate(curdate(),14)),
(18,23,curdate(),adddate(curdate(),6)),
(19,34,curdate(),adddate(curdate(),7)),
(20,29,curdate(),adddate(curdate(),4));

insert into UsuarioDieta (dieta,usuario,fechainicio,fechafin) values
(11,29,curdate(),adddate(curdate(),5)),
(12,23,curdate(),adddate(curdate(),7)),
(13,26,curdate(),adddate(curdate(),6)),
(14,23,curdate(),adddate(curdate(),3)),
(15,31,curdate(),adddate(curdate(),2)),
(16,34,curdate(),adddate(curdate(),1)),
(17,39,curdate(),adddate(curdate(),5)),
(18,32,curdate(),adddate(curdate(),3)),
(19,26,curdate(),adddate(curdate(),4)),
(20,21,curdate(),adddate(curdate(),10)),
(1,31,curdate(),adddate(curdate(),11)),
(2,27,curdate(),adddate(curdate(),3)),
(3,38,curdate(),adddate(curdate(),7)),
(4,40,curdate(),adddate(curdate(),15)),
(5,26,curdate(),adddate(curdate(),5)),
(6,37,curdate(),adddate(curdate(),7)),
(7,36,curdate(),adddate(curdate(),14)),
(8,34,curdate(),adddate(curdate(),6)),
(9,40,curdate(),adddate(curdate(),7)),
(10,29,curdate(),adddate(curdate(),4));


