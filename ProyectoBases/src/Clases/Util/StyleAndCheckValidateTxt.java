/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases.Util;

import Clases.InterfaceMethods.Validate;
import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.*;

/**
 *
 * @author Clases
 */
public class StyleAndCheckValidateTxt extends FocusAdapter{

    private final Border errorBorder ;
    private final Border defaultBorder;
    private JTextField text;
    
    public StyleAndCheckValidateTxt(JTextField text) {
        //SynthTextFieldUI.
        this.defaultBorder = text.getBorder();
        this.errorBorder = BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.RED, Color.getColor("RED", Color.TRANSLUCENT), Color.RED, Color.RED);
        //this.errorBorder = javax.swing.plaf.BorderUIResource.getLoweredBevelBorderUIResource();
        this.text = text;
    }
    
    
    @Override
    public void focusLost(FocusEvent e) {
        text.setBorder(defaultBorder);
        text.revalidate();
        text.repaint();
    }

    @Override
    public void focusGained(FocusEvent e) {
        text.setBorder(defaultBorder);
        text.revalidate();
        text.repaint();
    }
    
    public boolean isEmpty(){
        if(text.getText().isEmpty()){
            text.setBorder(errorBorder);
            text.revalidate();
            text.repaint();
            return true;
        }
        return false;
    }
    
    public boolean isOnlyAlfa(){
        if(!Validate.isOnlyAlfa(text.getText())){
            text.setBorder(errorBorder);
            text.revalidate();
            text.repaint();
            return false;
        }
        return true;
    }

    public boolean isOnlyAlfa(int validLength){
        if(!Validate.isOnlyAlfa(text.getText(), validLength)){
            text.setBorder(errorBorder);
            text.revalidate();
            text.repaint();
            return false;
        }
        return true;
    }   
    
    public boolean isOnlyNum(){
        if(!Validate.isOnlyNum(text.getText())){
            text.setBorder(errorBorder);
            text.revalidate();
            text.repaint();
            return false;
        }
        return true;
    }
    
    public boolean isOnlyNum(int validLength){
        if(!Validate.isOnlyNum(text.getText(), validLength)){
            text.setBorder(errorBorder);
            text.revalidate();
            text.repaint();
            return false;
        }
        return true;
    }
    
    public boolean isOnlyAlfaNum(){
        if(!Validate.isOnlyAlfaNum(text.getText())){
            text.setBorder(errorBorder);
            text.revalidate();
            text.repaint();
            return false;
        }
        return true;
    }
    
    public boolean isOnlyAlfaNum(int validLength){
        if(!Validate.isOnlyAlfaNum(text.getText(), validLength)){
            text.setBorder(errorBorder);
            text.revalidate();
            text.repaint();
            return false;
        }
        return true;
    }
}
