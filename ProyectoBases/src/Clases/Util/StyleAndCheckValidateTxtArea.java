/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases.Util;

import Clases.InterfaceMethods.Validate;
import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.BorderFactory;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

/**
 *
 * @author Clases
 */
public class StyleAndCheckValidateTxtArea extends FocusAdapter{
    private static final Border errorBorder = BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.RED, Color.getColor("RED", Color.TRANSLUCENT), Color.RED, Color.RED);;
    private final Border defaultBorder;
    private JTextArea text;

    public StyleAndCheckValidateTxtArea(JTextArea text) {
        this.text = text;
        defaultBorder = text.getBorder();
    }
    
    
    @Override
    public void focusLost(FocusEvent e) {
        text.setBorder(defaultBorder);
    }

    @Override
    public void focusGained(FocusEvent e) {
        text.setBorder(defaultBorder);
    }
    
    public boolean isEmpty(){
        if(text.getText().isEmpty()){
            text.setBorder(errorBorder);
            return true;
        }
        return false;
    }
    
    public boolean isOnlyAlfa(){
        if(Validate.isOnlyAlfa(text.getText())){
            text.setBorder(errorBorder);
            return true;
        }
        return false;
    }

    public boolean isOnlyAlfa(int validLength){
        if(Validate.isOnlyAlfa(text.getText(), validLength)){
            text.setBorder(errorBorder);
            return true;
        }
        return false;
    }   
    
    public boolean isOnlyNum(){
        if(Validate.isOnlyNum(text.getText())){
            text.setBorder(errorBorder);
            return true;
        }
        return false;
    }
    
    public boolean isOnlyNum(int validLength){
        if(Validate.isOnlyNum(text.getText(), validLength)){
            text.setBorder(errorBorder);
            return true;
        }
        return false;
    }
    
    public boolean isOnlyAlfaNum(){
        if(Validate.isOnlyAlfaNum(text.getText())){
            text.setBorder(errorBorder);
            return true;
        }
        return false;
    }
    
    public boolean isOnlyAlfaNum(int validLength){
        if(Validate.isOnlyAlfaNum(text.getText(), validLength)){
            text.setBorder(errorBorder);
            return true;
        }
        return false;
    }
    
}
