/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases.InterfaceMethods;

import Clases.Util.Messages;
import Controlador.AbstractController;
import javax.swing.JInternalFrame;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

/**
 *
 * @author Kevin
 */
public class InternalFrameList implements InternalFrameListener{

    private AbstractController controller;
    private final String[] opciones = {"Si", "No", "Cancelar"};
    private final String pregunta = "Desea guardar los cambios?";

    public InternalFrameList(AbstractController controller) {
        this.controller = controller;
    }
    
    @Override
    public void internalFrameOpened(InternalFrameEvent e) {
        
    }

    @Override
    public void internalFrameClosing(InternalFrameEvent e) {
        int op = Messages.questionMessage(pregunta, opciones);
        JInternalFrame frame = e.getInternalFrame();
        switch(op){
            case 0:{
                controller.save();
            }break;
            case 1:{
                frame.dispose();
            }break;
            case 2:{
            }
        }
    }

    @Override
    public void internalFrameClosed(InternalFrameEvent e) {
    }

    @Override
    public void internalFrameIconified(InternalFrameEvent e) {
        
    }

    @Override
    public void internalFrameDeiconified(InternalFrameEvent e) {
        
    }

    @Override
    public void internalFrameActivated(InternalFrameEvent e) {
        
    }

    @Override
    public void internalFrameDeactivated(InternalFrameEvent e) {
        
    }
    
}
