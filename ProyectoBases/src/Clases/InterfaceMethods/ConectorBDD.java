package Clases.InterfaceMethods;

import java.util.ArrayList;
import java.util.Observable;

public class ConectorBDD extends Observable {
    private String url; 
    private String dbName;
    private String userName; 
    private String password;
    private String host;

    private final String driver = "com.mysql.jdbc.Driver";
    
    private static final ConectorBDD instance = new ConectorBDD();
    
    private ConectorBDD() {
        host = "192.168.0.7";
        dbName = "BaseASIVISA";
        userName = "ASIVISA"; 
        password = "asivisa";
        url = "jdbc:mysql://" + host + "/" + dbName; 
    }
   
    public static ConectorBDD getInstance() {
        return instance;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setValues(ArrayList<String> text){
        if(text.size() > 4)
            System.out.println("Error mas datos de los que se pueden escribir");
        else{
            setHost(text.get(0));
            setDbName(text.get(1));
            setUserName(text.get(2));
            setPassword(text.get(3));
            
            url = "jdbc:mysql://" + host + "/" + dbName;
        }
        setChanged();
        notifyObservers(ConectorBDD.getInstance());
        
    }
    
    public void setValues(){
        setChanged();
        notifyObservers(ConectorBDD.getInstance());
    }
}
