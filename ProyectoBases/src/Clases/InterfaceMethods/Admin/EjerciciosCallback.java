/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases.InterfaceMethods.Admin;

import InterfacesAdministrador.Busquedas.EjercicioBusqueda;
import InterfacesAdministrador.ingresoDatos.EjercicioNuevoPanel;
import Clases.InterfaceMethods.InternalFrameList;
import Controlador.AbstractController;
import Controlador.Busqueda.AbstractControllBusqueda;
import Controlador.Busqueda.BusquedaControllerFactory;
import Controlador.ControladorConectar;
import Controlador.Ingreso.AbstractControllerFactory;
import Controlador.Ingreso.AbstractControllerIngreso;
import Controlador.Ingreso.ControllerEjercicio;
import Controlador.Ingreso.IngresoControllerFactory;
import InterfacesAdministrador.vtnNueva;
import Modelo.Busqueda.ResultSetTableModel;
import Modelo.Ejercicio;
import com.mysql.jdbc.ResultSetImpl;
import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

/**
 *
 * @author Kevin
 */
public class EjerciciosCallback {
    
    private static int frameCountNuevo = 0;
    private static int frameCountBusqueda = 0;
    private static int frameCountEjercicio = 0;
    
    public void mnuBtnNuevEjerActionPerformed(java.awt.event.ActionEvent evt, JDesktopPane ventana) {                                               
        // TODO add your handling code here:
        Ejercicio model = new Ejercicio(0, null, null, null, 0);
        AbstractControllerIngreso controller = IngresoControllerFactory.getInstance().getFactory("Ejercicio", "Ejercicio", model);
        InternalFrameList listener = new InternalFrameList(controller);
        vtnNueva vtn = vtnNueva.createWindow((JPanel)controller.getView(), "Ejercicio Nuevo ",(++frameCountNuevo));
        vtn.addInternalFrameListener(listener);
        
        ventana.add(vtn);
        
    }
    
    public void mnuBtnBusEjerciciosActionPerformed(java.awt.event.ActionEvent evt, JDesktopPane ventana) throws SQLException {                                                    
        // TODO add your handling code here:
        ResultSetTableModel model = new ResultSetTableModel(null,ControladorConectar.getInstance().isConnected());
        AbstractControllBusqueda controller = BusquedaControllerFactory.getInstance().getBusqueda("Ejercicio", "Ejercicio", model);
        
        vtnNueva vtn = vtnNueva.createWindow((JPanel)controller.getView(), "Busqueda de ejercicios" , (++frameCountBusqueda));
        
        InternalFrameList listener = new InternalFrameList(controller);
        vtn.addInternalFrameListener(listener);
        
        ventana.add(vtn);
        try{
            vtn.setSelected(true);
        }catch (java.beans.PropertyVetoException e){}
        
    }
}
