/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases.InterfaceMethods.Admin;

import Clases.InterfaceMethods.InternalFrameList;
import Controlador.Ingreso.AbstractControllerIngreso;
import Controlador.Ingreso.IngresoControllerFactory;
import InterfacesAdministrador.Busquedas.MembresiasBusquedas;
import InterfacesAdministrador.ingresoDatos.membresiaNuevoPanel;
import InterfacesAdministrador.reportes.MembresiaReportPanel;
import InterfacesAdministrador.vtnNueva;
import Modelo.TipoMembresia;
import java.math.BigDecimal;
import javax.swing.JDesktopPane;
import javax.swing.JPanel;

/**
 *
 * @author Clases
 */
public class MembresiaCallback {
    private static int frameCountNuevo = 0;
    private static int frameCountBusqueda = 0;
    private static int frameCountReporte = 0;
    
     
    public void mnuBtnNuevMembActionPerformed(java.awt.event.ActionEvent evt, JDesktopPane ventana) {                                               
        // TODO add your handling code here:
        TipoMembresia model = new TipoMembresia(0, null,0 , BigDecimal.ZERO );
        
        AbstractControllerIngreso controller = IngresoControllerFactory.getInstance().getFactory("Ejecicio", "Ejercicio", model);
        InternalFrameList listener = new InternalFrameList(controller);
        vtnNueva vtn = vtnNueva.createWindow((JPanel)controller.getView(), "Ejercicio Nuevo ",(++frameCountNuevo));
        vtn.addInternalFrameListener(listener);
        
        ventana.add(vtn);
        
        try{
            vtn.setSelected(true);
        }catch (java.beans.PropertyVetoException e){}
        
    }
    public void mnuMembBtnEstadoActionPerformed(java.awt.event.ActionEvent evt, JDesktopPane ventana) {                                                
        
        
    }                                               
}
