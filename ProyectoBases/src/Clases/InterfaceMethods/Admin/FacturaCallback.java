/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases.InterfaceMethods.Admin;

import Clases.InterfaceMethods.InternalFrameList;
import Controlador.Busqueda.AbstractControllBusqueda;
import Controlador.Busqueda.BusquedaControllerFactory;
import Controlador.Ingreso.AbstractControllerIngreso;
import Controlador.Ingreso.IngresoControllerFactory;
import InterfacesAdministrador.ingresoDatos.facturaNewPanel;
import InterfacesAdministrador.vtnNueva;
import Modelo.Factura;
import java.math.BigDecimal;
import javax.swing.JDesktopPane;
import javax.swing.JPanel;

/**
 *
 * @author Clases
 */
public class FacturaCallback {
    private static int frameCountNuevo = 0;
    private static int frameCountBusqueda = 0;
    private static int frameCountEjercicio = 0;
    
    public void mnuBtnNuevFacturaActionPerformed(java.awt.event.ActionEvent evt, JDesktopPane ventana) {                                               
        // TODO add your handling code here:
        Factura model = new Factura(0, null, null, BigDecimal.valueOf(0.0), null, true,BigDecimal.ZERO);
        
        AbstractControllerIngreso controller = IngresoControllerFactory.getInstance().getFactory("Factura", "Factura", model);
        InternalFrameList listener = new InternalFrameList(controller);
        vtnNueva vtn = vtnNueva.createWindow((JPanel)controller.getView(), "Ejercicio Nuevo ",(++frameCountNuevo));
        vtn.addInternalFrameListener(listener);
        
        ventana.add(vtn);
        
    }
}
