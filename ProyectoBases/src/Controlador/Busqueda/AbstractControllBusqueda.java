/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Busqueda;

import Clases.InterfaceMethods.ConnectDbInterface;
import Clases.Util.Messages;
import Controlador.AbstractController;
import InterfacesAdministrador.Busquedas.*;
import InterfacesAdministrador.vtnFormulario;
import Modelo.Busqueda.ResultSetTableModel;
import Modelo.modelDataBaseInterface;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.regex.PatternSyntaxException;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Clases
 */
public abstract class AbstractControllBusqueda extends AbstractController implements FocusListener, MouseListener, 
                                                                                      ActionListener, ConnectDbInterface{
    
    private JPopupMenu popup;
    
    protected Busqueda view;
    protected ResultSetTableModel model;
    protected JTable tabla;
    protected String[] data;
    protected String tipoDato;
    protected String[] nombres;
    
    public AbstractControllBusqueda(String nombreBusqueda, ResultSetTableModel model) {
        this.view = BusquedaFactory.getInstance().getBusqueda(nombreBusqueda, this, model);
        this.model = model;
        tabla = view.getTable();
        data = view.buscar();
    }

    public Busqueda getView() {
        return view;
    }

    public void setView(Busqueda view) {
        this.view = view;
    }

    public ResultSetTableModel getModel() {
        return model;
    }

    public void setModel(ResultSetTableModel model) {
        this.model = model;
    }
    
    
    @Override
    public void focusGained(FocusEvent e) {
    }
    
    @Override
    public void focusLost(FocusEvent e) {
        JTextField txt;
        if(e.getComponent() instanceof JTextField){
            txt = (JTextField) e.getComponent();
            
            String text = txt.getText();
            
            if(text.length() != 0){
                filtrarDatos(text);
            }
        }
    }

    public void setPopup(JPopupMenu popup) {
        this.popup = popup;
    }

    private void maybeShowPopup(MouseEvent e) {
        this.popup = view.getPopUpMenu();
        
        if (e.isPopupTrigger()) {
            popup.show(e.getComponent(),
                    e.getX(), e.getY());
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    private LinkedList<modelDataBaseInterface> getSelectedObjects(){
        int[] rows = tabla.getSelectedRows();
        LinkedList<modelDataBaseInterface> listDeObjBuscar = new LinkedList<>();
        
        if (rows.length != 0) {
            String acumula = null;
            for (int i : rows) {
                for (int j = 0; j < model.getColumnCount(); j++) {
                    acumula += model.getValueAt(i, j).toString();
                    acumula += ",";
                }
                acumula += "\n";
                listDeObjBuscar.add(makeFromObject(acumula));
            }
        } else {
            Messages.errorMessage("Por favor seleccione las filas a usar");
        }

        return listDeObjBuscar;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String text = e.getActionCommand();
        LinkedList<modelDataBaseInterface> list = getSelectedObjects();
        
        switch (text){
            case "Editar":{
                vtnFormulario vtn = new vtnFormulario(tipoDato, list);
                vtn.setVisible(true);
                
            }break;
            case "Eliminar":{
                int i = 0;
                for(modelDataBaseInterface modelList : list){
                    boolean flag;
                    
                    flag = eliminarDato(modelList);
                    if (!flag) {
                        Messages.errorMessage(String.format("No se pudo borrar el %d registro de la seleccion de la tabla", i));
                    }        
                }
            }break;
             
        }
    }

    @Override
    public boolean escribirDatosDB() {
        return false;
    }

    
    abstract protected modelDataBaseInterface makeFromObject(Object string);
    abstract protected boolean eliminarDato(modelDataBaseInterface model);
    abstract protected boolean filtrarDatos(String[] valores);
    abstract protected boolean filtrarDatos(String valor);
}
