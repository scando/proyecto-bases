/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Busqueda;

import Clases.Util.Messages;
import Modelo.Busqueda.ResultSetTableModel;
import Modelo.Ejercicio;
import Modelo.Intensidad;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Clases
 */
public class ControlerEjercicio extends AbstractControllBusqueda{

    public ControlerEjercicio(String nombreBusqueda, ResultSetTableModel model) {
        super(nombreBusqueda, model);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e); 
        StringTokenizer token;
        String actionComand = e.getActionCommand();
        switch(actionComand){
            case "Opcion1":{
                String busNom = Messages.inputMessage("Ingrese el nombre del o los ejercicios a buscar");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }
            }break;
            case "Opcion2":{
                
            }break;
            case "Opcion3":{
                String busNom = Messages.inputMessage("Ingrese el nombre del o las areas del cuerpo a buscar");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }
            }break;
            case "Opcion4":{
                String busNom = Messages.inputMessage("Ingrese el nivel de intensidad de los ejercicios a buscar");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }
            }break;
        }
    }

    @Override
    protected modelDataBaseInterface makeFromObject(Object string) {
        StringTokenizer tok = new StringTokenizer((String) string,",");
        Ejercicio ejer = null;
        
        if(tok.countTokens() == 5 ){
            int idEjercicio = Integer.getInteger(tok.nextToken());
            String nombre = tok.nextToken();
            String descripcion = tok.nextToken();
            Intensidad intensidad = null ;
            int intense = Integer.parseInt(tok.nextToken());
            try{
                storedProcedure = conexion.prepareCall("call cargarIntensidad(?)");
                storedProcedure.setInt(1, intense);
                resultSet = storedProcedure.executeQuery();
                if(resultSet.next()){
                    int id = resultSet.getInt(1);
                    String nombreIntensidad = resultSet.getString(2);
                    intensidad = new Intensidad(id, nombreIntensidad);
                }
                
            } catch (SQLException e){
                
            }
            int calorias = Integer.getInteger(tok.nextToken());
            
            ejer = new Ejercicio(idEjercicio, nombre, descripcion, intensidad, calorias);
        }
        
        return ejer;
    }

    @Override
    protected boolean eliminarDato(modelDataBaseInterface model) {
        try {
            Ejercicio ejercicio = (Ejercicio) model;
            storedProcedure = conexion.prepareCall("call eliminarEjercicio(?)");
            storedProcedure.setInt(1, ejercicio.getIdEjercicio());
            storedProcedure.execute();
        } catch (SQLException e) {
            Messages.errorMessage("Error:\n" + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    protected boolean filtrarDatos(String[] valores) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean filtrarDatos(String valor) {        
        try {
            storedProcedure = conexion.prepareCall("call busquedaEjercicioNombre(?)");
            storedProcedure.setString(1, valor);
            resultSet = storedProcedure.executeQuery();
            model.setResultSet(resultSet);
        } catch (SQLException ex) {
            Messages.errorMessage("Error:\n" + ex.getMessage());
            return false;
        }
        return true;        
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean leerDatosDB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
