/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Busqueda;

import Controlador.AbstractFactory;
import Modelo.Busqueda.ResultSetTableModel;

/**
 *
 * @author Clases
 */
public class BusquedaControllerFactory extends AbstractFactory{
    
    private BusquedaControllerFactory() {
    }
    
    public static BusquedaControllerFactory getInstance() {
        return BusquedaControllerFactoryHolder.INSTANCE;
    }

    @Override
    public AbstractControllBusqueda getBusqueda(String nombreControlador, String nombreVista, ResultSetTableModel model) {
        AbstractControllBusqueda buscar = null; 
        if (nombreControlador == null | nombreVista == null | model == null) {
            return null;
        }

        switch(nombreControlador){
            case "Usuario":{
                buscar = new ControllerUsuario(nombreVista, model);
            }break;
            case "Ejercicio":{
                buscar = new ControlerEjercicio(nombreVista, model);
            }break;
            case "Rutina":{
                buscar = new ControllerRutinas(nombreVista, model);
            }break;
            case "Membresia": {
                buscar = new ControllerTipoMembresia(nombreVista, model);
            }
            break;
            case "Factura": {
                buscar = new ControllerFacturas(nombreVista, model);
            }
            break;
            case "Dieta": {
                buscar = new ControllerDieta(nombreVista, model);
            }
            break;
            case "AreaDelCuerpo": {
                buscar = new ControllerAreasDelCuerpo(nombreVista, model);
            }
            break;
        }

        return buscar;
    }
    
    private static class BusquedaControllerFactoryHolder {

        private static final BusquedaControllerFactory INSTANCE = new BusquedaControllerFactory();
    }
    
    
}
