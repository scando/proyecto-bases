/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Busqueda;

import Clases.Util.Messages;
import Modelo.Busqueda.ResultSetTableModel;
import Modelo.Comida;
import Modelo.HoraComida;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;
import java.util.StringTokenizer;
import javax.swing.event.ListSelectionEvent;

/**
 *
 * @author Clases
 */
public class ControllerComida extends AbstractControllBusqueda{

    public ControllerComida(String nombreBusqueda, ResultSetTableModel model) {
        super(nombreBusqueda, model);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
 
        StringTokenizer token;
        String actionComand = e.getActionCommand();
        switch(actionComand){
            case "Opcion1":{
                String busNom = Messages.inputMessage("Ingrese el nombre de la comida");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");

                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }
            }break;
            case "Opcion2":{                
                String busNom = Messages.inputMessage("Ingrese las calorias de la comida");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");

                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }                
            }break;            
        }
    }

    
    @Override
    protected modelDataBaseInterface makeFromObject(Object string) {
        StringTokenizer tok = new StringTokenizer((String) string,",");
        Comida comida = null;
        
        if(tok.countTokens() == 6 ){
            int idComida = Integer.getInteger(tok.nextToken());
            int calorias = Integer.getInteger(tok.nextToken());
            String nombre = tok.nextToken();
            String descripcion = tok.nextToken();
            boolean platoFuerte = Boolean.getBoolean(tok.nextToken());
            HoraComida horaComida = null ;
            int hc = Integer.parseInt(tok.nextToken());
            try{
                storedProcedure = conexion.prepareCall("call cargarHoraComida(?)");
                storedProcedure.setInt(1, hc);
                resultSet = storedProcedure.executeQuery();
                if(resultSet.next()){
                    int id = resultSet.getInt(1);
                    String hora = resultSet.getString(2);
                    horaComida = new HoraComida(id, hora);
                }                
            } catch (SQLException e){
                
            }                        
            comida = new Comida(idComida, calorias, nombre, descripcion, platoFuerte, horaComida);
        }        
        return comida;
    }

    @Override
    protected boolean eliminarDato(modelDataBaseInterface model) {
        try {
            Comida comida = (Comida) model;
            storedProcedure = conexion.prepareCall("call eliminarComida(?)");
            storedProcedure.setInt(1, comida.getIdComida());
            storedProcedure.execute();
        } catch (SQLException e) {
            Messages.errorMessage("Error:\n" + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    protected boolean filtrarDatos(String[] valores) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean filtrarDatos(String valor) {
        try {
            storedProcedure = conexion.prepareCall("call busquedaComidaNombre(?)");
            storedProcedure.setString(1, valor);
            resultSet = storedProcedure.executeQuery();
            model.setResultSet(resultSet);
        } catch (SQLException ex) {
            Messages.errorMessage("Error:\n" + ex.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean leerDatosDB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
