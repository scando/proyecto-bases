/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Busqueda;

import Clases.Util.Messages;
import Modelo.Busqueda.ResultSetTableModel;
import Modelo.Factura;
import Modelo.Usuario;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.util.StringTokenizer;

/**
 *
 * @author Clases
 */
public class ControllerFacturas extends AbstractControllBusqueda{

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e); 
        StringTokenizer token;
        String actionComand = e.getActionCommand();
        switch(actionComand){
            case "Opcion1":{
                String busNom = Messages.inputMessage("Ingrese tipo de membresia a buscar");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }
            }break;
            case "Opcion2":{
                String busNom = Messages.inputMessage("Ingrese el costo de la membresia");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }                
            }break;
        }        
    }

    public ControllerFacturas(String nombreBusqueda, ResultSetTableModel model) {
        super(nombreBusqueda, model);
    }

    @Override
    protected modelDataBaseInterface makeFromObject(Object string) {
        StringTokenizer tok = new StringTokenizer((String) string,",");
        Factura fac = null;
        if(tok.countTokens() == 7 ){
            int idFactura=Integer.getInteger(tok.nextToken());
            Date fechaEmision = Date.valueOf(tok.nextToken());
            Date fechaCredito = Date.valueOf(tok.nextToken());
            BigDecimal monto = BigDecimal.valueOf(Integer.getInteger(tok.nextToken()));
            Usuario usuario = null;
            int u = Integer.parseInt(tok.nextToken());
            try{
                storedProcedure = conexion.prepareCall("call cargarUsuario(?)");
                storedProcedure.setInt(1, u);
                resultSet = storedProcedure.executeQuery();
                if (resultSet.next()) {
                    int id = resultSet.getInt(1);
                    String nombre = resultSet.getString(2);
                    String cedula = resultSet.getString(3);
                    String username = resultSet.getString(4);
                    String contrasena = resultSet.getString(5);
                    String email = resultSet.getString(6);
                    String residencia = resultSet.getString(7);
                    String pais = resultSet.getString(8);
                    String ciudad = resultSet.getString(9);
                    int pesoInicial = resultSet.getInt(10);
                    int pesoActual = resultSet.getInt(11);
                    int pesoMeta = resultSet.getInt(12);
                    int estatura = resultSet.getInt(13);
                    boolean perfilPublico = resultSet.getBoolean(14);
                    Date fechaInicioMembresia = resultSet.getDate(16);
                    Date fechaFinMembresia = resultSet.getDate(17);                   
                    usuario = new Usuario(id, nombre, cedula, username, contrasena, email, residencia, pais, ciudad, pesoInicial, pesoActual, pesoMeta, estatura, perfilPublico, null, fechaInicioMembresia, fechaFinMembresia);
                }
                
            } catch (SQLException e){
                
            }
            boolean valido=Boolean.getBoolean(tok.nextToken());
            BigDecimal saldo= BigDecimal.valueOf(Integer.getInteger(tok.nextToken()));;
            fac = new Factura(idFactura, fechaEmision, fechaCredito, monto, usuario, valido, saldo);
        }       
        return fac;
    }

    @Override
    protected boolean eliminarDato(modelDataBaseInterface model) {
        try {
            Factura factura = (Factura) model;
            storedProcedure = conexion.prepareCall("call anularFactura(?)");
            storedProcedure.setInt(1, factura.getIdFactura());
            storedProcedure.execute();
        } catch (SQLException e) {
            Messages.errorMessage("Error:\n" + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    protected boolean filtrarDatos(String[] valores) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean filtrarDatos(String valor) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean leerDatosDB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
