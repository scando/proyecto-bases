/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador.Busqueda;

import Clases.Util.Messages;
import Modelo.Busqueda.ResultSetTableModel;
import Modelo.HoraComida;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;
import java.util.StringTokenizer;

/**
 *
 * @author Maro
 */
public class ControllerHoraComida extends AbstractControllBusqueda{

    public ControllerHoraComida(String nombreBusqueda, ResultSetTableModel model) {
        super(nombreBusqueda, model);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected modelDataBaseInterface makeFromObject(Object string) {       
        StringTokenizer tok = new StringTokenizer((String) string,",");
        HoraComida hc = null;        
        if(tok.countTokens() == 2 ){
            int idhc = Integer.getInteger(tok.nextToken());
            String nombre = tok.nextToken();            
            hc = new HoraComida(idhc, nombre);
        }        
        return hc;
    }

    @Override
    protected boolean eliminarDato(modelDataBaseInterface model) {
        try {
            HoraComida hc = (HoraComida) model;
            storedProcedure = conexion.prepareCall("call eliminarADC(?)");
            storedProcedure.setInt(1, hc.getIdHoraComida());
            storedProcedure.execute();
        } catch (SQLException e) {
            Messages.errorMessage("Error:\n" + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    protected boolean filtrarDatos(String[] valores) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean filtrarDatos(String valor) {
        try {
            storedProcedure = conexion.prepareCall("call busquedaHoraComida(?)");
            storedProcedure.setString(1, valor);
            resultSet = storedProcedure.executeQuery();
            model.setResultSet(resultSet);
        } catch (SQLException ex) {
            Messages.errorMessage("Error:\n" + ex.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics grphcs, PageFormat pf, int i) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean leerDatosDB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
