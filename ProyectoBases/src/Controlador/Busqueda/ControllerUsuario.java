/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Busqueda;

import Clases.Util.Messages;
import Controlador.AbstractController;
import InterfacesAdministrador.Busquedas.BusquedaFactory;
import InterfacesAdministrador.Busquedas.UsuariosBusquedas;
import Modelo.Busqueda.ResultSetTableModel;
import Modelo.TipoMembresia;
import Modelo.Usuario;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.util.StringTokenizer;
import javax.swing.JTable;

/**
 *
 * @author Clases
 */
public class ControllerUsuario extends AbstractControllBusqueda {

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        StringTokenizer token;
        String actionComand = e.getActionCommand();
        switch (actionComand) {
            case "Opcion1": {
                String busNom = Messages.inputMessage("Ingrese el nombre del usuario");
                if (!busNom.isEmpty()) {
                    token = new StringTokenizer(busNom, "|");

                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while (token.hasMoreElements()) {
                        nombres[i] = token.nextToken();
                        i++;
                    }
                }
            }
            break;
            case "Opcion2": {

                String busNom = Messages.inputMessage("Ingrese el lugar de residencia");
                if (!busNom.isEmpty()) {
                    token = new StringTokenizer(busNom, "|");

                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while (token.hasMoreElements()) {
                        nombres[i] = token.nextToken();
                        i++;
                    }
                }

            }
            break;
            case "Opcion3": {
                String busNom = Messages.inputMessage("Ingrese el peso inicial");
                if (!busNom.isEmpty()) {
                    token = new StringTokenizer(busNom, "|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while (token.hasMoreElements()) {
                        nombres[i] = token.nextToken();
                        i++;
                    }
                }
            }
            break;
            case "Opcion4": {

            }
            break;

        }
    }

    public ControllerUsuario(String nombreBusqueda, ResultSetTableModel model) {
        super(nombreBusqueda, model);

    }

    @Override
    protected modelDataBaseInterface makeFromObject(Object string) {
        StringTokenizer tok = new StringTokenizer((String) string, ",");
        Usuario usuario = null;

        if (tok.countTokens() == 17) {
            int idUsuario = Integer.getInteger(tok.nextToken());
            String nombre = tok.nextToken();
            String cedula = tok.nextToken();
            String userName = tok.nextToken();
            String contrasena = tok.nextToken();            
            String email = tok.nextToken();
            String residencia = tok.nextToken();
            String pais = tok.nextToken();
            String ciudad = tok.nextToken();
            int pesoInicial = Integer.getInteger(tok.nextToken());
            int pesoActual = Integer.getInteger(tok.nextToken());
            int pesoMeta = Integer.getInteger(tok.nextToken());
            int estatura =Integer.getInteger(tok.nextToken());
            boolean perfilPublico = Boolean.getBoolean(tok.nextToken());
            TipoMembresia tipoMembresia = null;
            int tipoM = Integer.getInteger(tok.nextToken());
            try {
                storedProcedure = conexion.prepareCall("call cargartipoMembresia(?)");
                storedProcedure.setInt(1, tipoM);
                resultSet = storedProcedure.executeQuery();
                if (resultSet.next()) {
                    int id = resultSet.getInt(1);
                    String nombreTipoMembresia = resultSet.getString(2);
                    int diasVigencia =resultSet.getInt(3);
                    BigDecimal costoMembresia = resultSet.getBigDecimal(4);
                    tipoMembresia = new TipoMembresia(id, nombreTipoMembresia, diasVigencia, costoMembresia);
                }
            } catch (SQLException e) {

            }
            Date fechaInicio = Date.valueOf(tok.nextToken());
            Date fechaFin = Date.valueOf(tok.nextToken());
            usuario = new Usuario(idUsuario, nombre, userName, contrasena, cedula, email, residencia, pais, ciudad, pesoInicial, pesoActual, pesoMeta, estatura, perfilPublico, tipoMembresia, fechaInicio, fechaFin);
        }
        return usuario;
    }

    @Override
        protected boolean eliminarDato(modelDataBaseInterface model) {
        try {
            Usuario usuario = (Usuario) model;
            storedProcedure = conexion.prepareCall("call eliminarUsuario(?)");
            storedProcedure.setInt(1, usuario.getIdUsuario());
            storedProcedure.execute();
        } catch (SQLException e) {
            Messages.errorMessage("Error:\n" + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
        protected boolean filtrarDatos(String[] valores) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean filtrarDatos(String valor) {
        try {
            storedProcedure = conexion.prepareCall("call busquedaUsuarioNombre(?)");
            storedProcedure.setString(1, valor);
            resultSet = storedProcedure.executeQuery();
            model.setResultSet(resultSet);
        } catch (SQLException ex) {
            Messages.errorMessage("Error:\n" + ex.getMessage());
            return false;
        }
        return true;
    }

    @Override
        public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
        public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
        public boolean leerDatosDB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

    
}
