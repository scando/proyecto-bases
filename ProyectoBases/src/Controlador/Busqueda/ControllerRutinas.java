/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Busqueda;

import Clases.Util.Messages;
import Modelo.Busqueda.ResultSetTableModel;
import Modelo.Rutina;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;
import java.util.StringTokenizer;

/**
 *
 * @author Clases
 */
public class ControllerRutinas extends AbstractControllBusqueda{

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        StringTokenizer token;
        String actionComand = e.getActionCommand();
        switch(actionComand){
            case "Opcion1":{
                String busNom = Messages.inputMessage("Ingrese el año de la rutina a buscar");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");

                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }
            }break;
        }
    }

    public ControllerRutinas(String nombreBusqueda, ResultSetTableModel model) {
        super(nombreBusqueda, model);
    }

    @Override
    protected modelDataBaseInterface makeFromObject(Object string) {
        StringTokenizer tok = new StringTokenizer((String) string,",");
        Rutina rut = null;
        
        if(tok.countTokens() == 3 ){
            int idRutina = Integer.getInteger(tok.nextToken());
            String nombre = tok.nextToken();
            String descripcion = tok.nextToken();            
            rut = new Rutina(idRutina, nombre, descripcion);
        }
        
        return rut;
    }

    @Override
    protected boolean eliminarDato(modelDataBaseInterface model) {
        try {
            Rutina rutina = (Rutina) model;
            storedProcedure = conexion.prepareCall("call eliminarEjercicio(?)");
            storedProcedure.setInt(1, rutina.getIdRutina());
            storedProcedure.execute();
        } catch (SQLException e) {
            Messages.errorMessage("Error:\n" + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    protected boolean filtrarDatos(String[] valores) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean filtrarDatos(String valor) {
        try {
            storedProcedure = conexion.prepareCall("call busquedaRutinaNombre(?)");
            storedProcedure.setString(1, valor);
            resultSet = storedProcedure.executeQuery();
            model.setResultSet(resultSet);
        } catch (SQLException ex) {
            Messages.errorMessage("Error:\n" + ex.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean leerDatosDB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
