/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Busqueda;

import Clases.Util.Messages;
import Modelo.AreaDelCuerpo;
import Modelo.Busqueda.ResultSetTableModel;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;
import java.util.StringTokenizer;
import javax.swing.event.ListSelectionEvent;

/**
 *
 * @author Clases
 */
public class ControllerAreasDelCuerpo extends AbstractControllBusqueda{

    public ControllerAreasDelCuerpo(String nombreBusqueda, ResultSetTableModel model) {
        super(nombreBusqueda, model);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e); //To change body of generated methods, choose Tools | Templates.
        StringTokenizer token;
        String actionComand = e.getActionCommand();
        switch(actionComand){
            case "Opcion1":{
                String busNom = Messages.inputMessage("Ingrese tipo de membresia a buscar");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }
            }break;
            case "Opcion2":{
                String busNom = Messages.inputMessage("Ingrese el costo de la membresia");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }                
            }break;
        }
    }

    @Override
    protected modelDataBaseInterface makeFromObject(Object string) {
        StringTokenizer tok = new StringTokenizer((String) string,",");
        AreaDelCuerpo adc = null;
        
        if(tok.countTokens() == 2 ){
            int idADC = Integer.getInteger(tok.nextToken());
            String nombre = tok.nextToken();            
            adc = new AreaDelCuerpo(idADC, nombre);
        }        
        return adc;
    }

    @Override
    protected boolean eliminarDato(modelDataBaseInterface model) {
        try {
            AreaDelCuerpo adc = (AreaDelCuerpo) model;
            storedProcedure = conexion.prepareCall("call eliminarADC(?)");
            storedProcedure.setInt(1, adc.getIdADC());
            storedProcedure.execute();
        } catch (SQLException e) {
            Messages.errorMessage("Error:\n" + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    protected boolean filtrarDatos(String[] valores) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean filtrarDatos(String valor) {
        try {
            storedProcedure = conexion.prepareCall("call busquedaADCNombre(?)");
            storedProcedure.setString(1, valor);
            resultSet = storedProcedure.executeQuery();
            model.setResultSet(resultSet);
        } catch (SQLException ex) {
            Messages.errorMessage("Error:\n" + ex.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean leerDatosDB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
}
