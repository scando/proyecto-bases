/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Busqueda;

import Clases.Util.Messages;
import Modelo.Busqueda.ResultSetTableModel;
import Modelo.Dieta;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;
import java.util.StringTokenizer;

/**
 *
 * @author Clases
 */
public class ControllerDieta extends AbstractControllBusqueda{

    public ControllerDieta(String nombreBusqueda, ResultSetTableModel model) {
        super(nombreBusqueda, model);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e); 
        StringTokenizer token;
        String actionComand = e.getActionCommand();
        switch(actionComand){
            case "Opcion1":{
                String busNom = Messages.inputMessage("Ingrese tipo de membresia a buscar");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }
            }break;
            case "Opcion2":{
                String busNom = Messages.inputMessage("Ingrese el costo de la membresia");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }                
            }break;
        }        
    }

    @Override
    protected modelDataBaseInterface makeFromObject(Object string) {
        StringTokenizer tok = new StringTokenizer((String) string,",");
        Dieta dieta = null;
        
        if(tok.countTokens() == 3 ){
            int idDieta = Integer.getInteger(tok.nextToken());
            String nombre = tok.nextToken();
            int ingestaCalorica = Integer.getInteger(tok.nextToken());
            dieta = new Dieta(idDieta, nombre, ingestaCalorica);
        }        
        return dieta;
    }

    @Override
    protected boolean eliminarDato(modelDataBaseInterface model) {
        try {
            Dieta dieta = (Dieta) model;
            storedProcedure = conexion.prepareCall("call eliminarEjercicio(?)");
            storedProcedure.setInt(1, dieta.getIdDieta());
            storedProcedure.execute();
        } catch (SQLException e) {
            Messages.errorMessage("Error:\n" + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    protected boolean filtrarDatos(String[] valores) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean filtrarDatos(String valor) {
        try {
            storedProcedure = conexion.prepareCall("call busquedaDietaNombre(?)");
            storedProcedure.setString(1, valor);
            resultSet = storedProcedure.executeQuery();
            model.setResultSet(resultSet);
        } catch (SQLException ex) {
            Messages.errorMessage("Error:\n" + ex.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
    }

    @Override
    public boolean leerDatosDB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
