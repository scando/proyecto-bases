/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador.Busqueda;

import Clases.Util.Messages;
import Modelo.Busqueda.ResultSetTableModel;
import Modelo.Factura;
import Modelo.Pago;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.util.StringTokenizer;

/**
 *
 * @author Maro
 */
public class ControllerPago extends AbstractControllBusqueda {

    public ControllerPago(String nombreBusqueda, ResultSetTableModel model) {
        super(nombreBusqueda, model);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected modelDataBaseInterface makeFromObject(Object string) {
        StringTokenizer tok = new StringTokenizer((String) string,",");
        Pago p = null;
        
        if (tok.countTokens() == 6) {
            int idPago = Integer.getInteger(tok.nextToken());
            Date fecha = Date.valueOf(tok.nextToken());
            Factura factura = null;
            int fac = Integer.parseInt(tok.nextToken());
            try {
                storedProcedure = conexion.prepareCall("call cargarFactura(?)");
                storedProcedure.setInt(1, fac);
                resultSet = storedProcedure.executeQuery();
                if (resultSet.next()) {
                    int id = resultSet.getInt(1);
                    Date fechaEmision = resultSet.getDate(2);
                    Date fechaCredito = resultSet.getDate(3);
                    BigDecimal monto = resultSet.getBigDecimal(4);
                    boolean valido = resultSet.getBoolean(6);
                    BigDecimal saldo = resultSet.getBigDecimal(7);
                    factura = new Factura(idPago, fechaEmision, fechaCredito, monto, null, valido, saldo);
                }

            } catch (SQLException e) {

            }
            String descripcion = tok.nextToken();
            BigDecimal monto = BigDecimal.valueOf(Float.parseFloat(tok.nextToken()));
            boolean valido = Boolean.getBoolean(tok.nextToken());
            p=new Pago(idPago, fecha, factura, descripcion, monto, valido);
        }
        return p;
    }

    @Override
    protected boolean eliminarDato(modelDataBaseInterface model) {
        try {
            Pago pago = (Pago) model;
            storedProcedure = conexion.prepareCall("call anularPago(?)");
            storedProcedure.setInt(1, pago.getIdPago());
            storedProcedure.execute();
        } catch (SQLException e) {
            Messages.errorMessage("Error:\n" + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    protected boolean filtrarDatos(String[] valores) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean filtrarDatos(String valor) {
        try {
            storedProcedure = conexion.prepareCall("call buscaPagosUsuarioNombre(?)");
            storedProcedure.setString(1, valor);
            resultSet = storedProcedure.executeQuery();
            model.setResultSet(resultSet);
        } catch (SQLException ex) {
            Messages.errorMessage("Error:\n" + ex.getMessage());
            return false;
        }
        return true; 
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics grphcs, PageFormat pf, int i) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean leerDatosDB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
