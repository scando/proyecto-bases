/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Clases.InterfaceMethods.ConectorBDD;
import Clases.Util.Messages;
import InterfacesAdministrador.Preferencias;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Clases
 */
public class ControladorConectar implements ActionListener, ComponentListener{

    private Connection con;
    private ConectorBDD model = ConectorBDD.getInstance();
    private static Preferencias view;
    private Boolean connectedToDatabase = false;
    private static ArrayList<String> defaultValues;
    
    private static ControladorConectar instance = new ControladorConectar();  
        

    private ControladorConectar() {
        try {
            conectar();
        } catch (SQLException ex) {
            Logger.getLogger(ControladorConectar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private ControladorConectar(Preferencias vista) {
        view = vista;
        model.addObserver(view);
        view.addControlerConection(instance);
        view.txtFieldSetEnable(true);
    }
    
    public static ControladorConectar getInstance(){
        
        instance.model.addObserver(view);
        view.addControlerConection(instance);
        view.txtFieldSetEnable(true);
        
        return instance;
    }
    
    public static Connection getConexion(){
        return instance.con;
    }

    public static void setView(Preferencias view) {
        ControladorConectar.view = view;
    }

    public static Preferencias getView() {
        return view;
    }
    
    
    
    private void conectar() throws SQLException {

        con = DriverManager.getConnection(model.getUrl(), model.getUserName(), model.getPassword());
        
        connectedToDatabase = true;
        
    }

    public void desconectar() throws SQLException {
        if(connectedToDatabase){
            con.close();
            connectedToDatabase = false;
            
        }
    }
    
    public boolean isConnected(){
        try {
            return con.isValid(5);
        } catch (SQLException ex) {
            
        }
        return false;
    }
    
    public void listaTablas() throws SQLException {
        ArrayList<String> tablas = new ArrayList<>();
        try {
            PreparedStatement ps = con.prepareStatement("show tables");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                tablas.add(rs.getString(1));
            }
            System.out.println("Se encontraron " + tablas.size() + " tablas");
            for (String tabla : tablas) {
                System.out.println(tabla);
            }
        } catch (SQLException e) {
            System.out.println("Error al listar las tablas");
            throw e;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Editar":{
                ControladorConectar.defaultValues = view.txtFieldGetText();
        
                view.btnSetEnable(false, true);
                view.txtFieldSetEditable(true);
            }break;
            case "Guardar":{
                ArrayList<String> txtlist = view.txtFieldGetText();
                view.txtFieldSetText("");
                model.setValues(txtlist);
                view.txtFieldSetEditable(false);
                try {
                    desconectar();
                } catch (SQLException ex) {
                    Messages.errorMessage("No esta conectado a la base de datos");
                }
                
                try {
                    conectar();
                } catch (SQLException ex) {
                    Messages.errorMessage("Revise los datos de conexion, no se pudo conectar");
                    model.setValues(defaultValues);
                }

                view.btnSetEnable(true, false);
                view.revalidate();
                view.repaint();
            }break;
        }
    }

    
    @Override
    public void componentResized(ComponentEvent e) {
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {
        model.setValues();
        view.revalidate();
        view.repaint();
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }
}
