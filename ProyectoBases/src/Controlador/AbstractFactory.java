/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Controlador.Busqueda.AbstractControllBusqueda;
import Modelo.Busqueda.ResultSetTableModel;

/**
 *
 * @author Clases
 */
public abstract class AbstractFactory {
    abstract public AbstractControllBusqueda getBusqueda(String nombreControlador, String nombreVista, ResultSetTableModel model);
}
