/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador.Ingreso;

import Clases.Util.Messages;
import InterfacesAdministrador.ingresoDatos.DietaNewPanel;
import InterfacesAdministrador.ingresoDatos.Ingreso;
import Modelo.Dieta;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;

/**
 *
 * @author Maro
 */
public class ControllerDieta extends AbstractControllerIngreso{

    public ControllerDieta(DietaNewPanel view, Dieta model) {
        super(view, model);
    }

    @Override
    public boolean escribirDatosDB() {
        if (view.verificarDatos()) {
            try {
                Dieta dieta = (Dieta) view.getModelFromView();
                storedProcedure = conexion.prepareCall("call grabaDieta(?,?)");
                storedProcedure.setInt(1, dieta.getIdDieta());
                storedProcedure.setString(2, dieta.getNombre());                                
                resultSet = storedProcedure.executeQuery();
                if (dieta.getIdDieta()== 0 && resultSet.next()) {
                    dieta.setIdDieta(resultSet.getInt(1));
                }
            } catch (SQLException e) {
                System.out.println("Error:\n" + e.getMessage());
                return false;
            }
            view.Actualizar();
            return true;
        }
        return false;
    }

    @Override
    public boolean leerDatosDB() {
        return false;
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        if (!escribirDatosDB()) {
            Messages.errorMessage("No se pudo Guardar en la base de datos");
        }
    }
    
}
