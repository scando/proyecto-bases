/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Ingreso;

import Clases.InterfaceMethods.ConnectDbInterface;
import Controlador.AbstractController;
import InterfacesAdministrador.ingresoDatos.Ingreso;
import Modelo.modelDataBaseInterface;

/**
 *
 * @author Clases
 */
public abstract class AbstractControllerIngreso extends AbstractController implements ConnectDbInterface{
    
    protected Ingreso view;
    protected modelDataBaseInterface model;

    
    public AbstractControllerIngreso(Ingreso view, modelDataBaseInterface model) {
        super();
        this.view = view;
        this.model = model;
        
        view.setViewFromModel(model);
    }   

    public Ingreso getView() {
        return view;
    }

    public void setView(Ingreso view) {
        this.view = view;
    }

    public modelDataBaseInterface getModel() {
        return model;
    }

    public void setModel(modelDataBaseInterface model) {
        this.model = model;
    }
}
