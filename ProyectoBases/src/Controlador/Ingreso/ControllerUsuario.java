/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Ingreso;

import Clases.Util.Messages;
import InterfacesAdministrador.ingresoDatos.Ingreso;
import InterfacesAdministrador.ingresoDatos.userNewPanel;
import Modelo.Usuario;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;

/**
 *
 * @author Maro
 */
public class ControllerUsuario extends AbstractControllerIngreso {

    public ControllerUsuario(userNewPanel view, Usuario model) {
        super(view, model);
    }

    @Override
    public boolean escribirDatosDB() {
        if (view.verificarDatos()) {
            try {
                Usuario usuario = (Usuario) view.getModelFromView();
                storedProcedure = conexion.prepareCall("call grabarUsuario(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");//IN id INT, IN nom VARCHAR(39), IN ced VARCHAR(20), IN usrname VARCHAR(20), IN contr VARCHAR(20), IN emailu VARCHAR(30), IN resid varchar(30), in pai varchar(30), in ciu varchar(30), in pesoI int, in pesoM int, in estat int, in perfilP tinyint(1), in tipoM int            
                storedProcedure.setInt(1, usuario.getIdUsuario());
                storedProcedure.setString(2, usuario.getNombre());
                storedProcedure.setString(3, usuario.getCedula());
                storedProcedure.setString(4, usuario.getUserName());
                storedProcedure.setString(5, usuario.getContrasena());
                storedProcedure.setString(6, usuario.getEmail());
                storedProcedure.setString(7, usuario.getResidencia());
                storedProcedure.setString(8, usuario.getPais());
                storedProcedure.setString(9, usuario.getCiudad());
                storedProcedure.setInt(10, usuario.getPesoInicial());
                storedProcedure.setInt(11, usuario.getPesoActual());
                storedProcedure.setInt(12, usuario.getPesoMeta());
                storedProcedure.setInt(13, usuario.getEstatura());
                storedProcedure.setBoolean(14, usuario.isPerfilPublico());
                storedProcedure.setInt(14, usuario.getTipoMembresia().getIdTipoMembresia());
                resultSet = storedProcedure.executeQuery();
                if (usuario.getIdUsuario() == 0 && resultSet.next()) {
                    usuario.setIdUsuario(resultSet.getInt(1));
                }
            } catch (SQLException e) {
                Messages.errorMessage("Error:\n" + e.getMessage());
                return false;
            }
            view.Actualizar();
            return true;
        }
        return false;
    }

    @Override
    public boolean leerDatosDB() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics grphcs, PageFormat pf, int i) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        if (!escribirDatosDB()) {
            Messages.errorMessage("No se pudo Guardar en la base de datos");
        }
    }

}
