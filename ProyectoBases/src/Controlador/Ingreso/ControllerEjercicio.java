/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Ingreso;

import Clases.Util.Messages;
import InterfacesAdministrador.ingresoDatos.EjercicioNuevoPanel;
import InterfacesAdministrador.ingresoDatos.Ingreso;
import Modelo.Ejercicio;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;

/**
 *
 * @author Clases
 */
public class ControllerEjercicio extends AbstractControllerIngreso{

    public ControllerEjercicio(EjercicioNuevoPanel view, Ejercicio model) {
        super(view, model);
    }

    @Override
    public boolean escribirDatosDB() {
         if (view.verificarDatos()){
            try {
                Ejercicio ejercicio = (Ejercicio) view.getModelFromView();
                storedProcedure = conexion.prepareCall("call grabarEjercicio(?,?,?,?,?)");
                storedProcedure.setInt(1, ejercicio.getIdEjercicio());
                storedProcedure.setString(2, ejercicio.getNombre());
                storedProcedure.setString(3, ejercicio.getDescripcion());
                storedProcedure.setInt(4, ejercicio.getIntensidad().getIdIntensidad());
                storedProcedure.setInt(5, ejercicio.getCalorias());
                resultSet = storedProcedure.executeQuery();
                if (ejercicio.getIdEjercicio() == 0 && resultSet.next()) {
                    ejercicio.setIdEjercicio(resultSet.getInt(1));
                }
            } catch (SQLException e) {
                System.out.println("Error:\n" + e.getMessage());
                return false;
            }
            view.Actualizar();
            return true;
        }
        return false;    
    }

    @Override
    public boolean leerDatosDB() {
        return false;
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        if (!escribirDatosDB()) {
            Messages.errorMessage("No se pudo Guardar en la base de datos");
        }
    }
    
}
