/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Ingreso;

import Modelo.modelDataBaseInterface;

/**
 *
 * @author Clases
 */
public abstract class AbstractControllerFactory {
    abstract public AbstractControllerIngreso getFactory(String nombreControlador, String nombreVista, modelDataBaseInterface model);
}
