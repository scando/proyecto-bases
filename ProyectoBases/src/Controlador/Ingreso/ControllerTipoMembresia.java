/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador.Ingreso;

import Clases.Util.Messages;
import InterfacesAdministrador.ingresoDatos.Ingreso;
import InterfacesAdministrador.ingresoDatos.TipoMembresiaNuevoPanel;
import Modelo.TipoMembresia;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;

/**
 *
 * @author Maro
 */
public class ControllerTipoMembresia extends AbstractControllerIngreso{

    public ControllerTipoMembresia(TipoMembresiaNuevoPanel view, TipoMembresia model) {
        super(view, model);
    }

    @Override
    public boolean escribirDatosDB() {
        if (view.verificarDatos()){
            try {
                TipoMembresia tipoMembresia = (TipoMembresia) view.getModelFromView();
                storedProcedure = conexion.prepareCall("call grabarTipoMembresia(?,?,?,?)");
                storedProcedure.setInt(1, tipoMembresia.getIdTipoMembresia());
                storedProcedure.setString(2, tipoMembresia.getNombreTipoMembresia());
                storedProcedure.setInt(3, tipoMembresia.getDiasVigencia());
                storedProcedure.setBigDecimal(4, tipoMembresia.getCosto());
                resultSet = storedProcedure.executeQuery();
                if (tipoMembresia.getIdTipoMembresia()== 0 && resultSet.next()) {
                    tipoMembresia.setIdTipoMembresia(resultSet.getInt(1));
                }
            } catch (SQLException e) {
                System.out.println("Error:\n" + e.getMessage());
                return false;
            }
            view.Actualizar();
            return true;
        }
        return false;
    }

    @Override
    public boolean leerDatosDB() {
        return false;
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics grphcs, PageFormat pf, int i) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        if (!escribirDatosDB()) {
            Messages.errorMessage("No se pudo Guardar en la base de datos");
        }
    }
    
}
