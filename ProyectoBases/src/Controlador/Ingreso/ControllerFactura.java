/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador.Ingreso;

import InterfacesAdministrador.ingresoDatos.Ingreso;
import InterfacesAdministrador.ingresoDatos.facturaNewPanel;
import Modelo.Factura;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;

/**
 *
 * @author Maro
 */
public class ControllerFactura extends AbstractControllerIngreso{

    public ControllerFactura(facturaNewPanel view, Factura model) {
        super(view, model);
    }

    @Override
    public boolean escribirDatosDB() {
        if (view.verificarDatos()){
            try {
                Factura factura = (Factura) view.getModelFromView();
                storedProcedure = conexion.prepareCall("call grabarFactura(?)");
                storedProcedure.setInt(1, factura.getUsuario().getIdUsuario());                
                resultSet = storedProcedure.executeQuery();
                if (resultSet.next()) {
                    factura.setIdFactura(resultSet.getInt(1));
                }
            } catch (SQLException e) {
                System.out.println("Error:\n" + e.getMessage());
                return false;
            }
            view.Actualizar();
            return true;
        }
        return false;
    }

    @Override
    public boolean leerDatosDB() {
        return false;
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
