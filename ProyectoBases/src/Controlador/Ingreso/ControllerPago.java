/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador.Ingreso;

import Clases.Util.Messages;
import InterfacesAdministrador.ingresoDatos.Ingreso;
import Modelo.Pago;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;

/**
 *
 * @author Maro
 */
public class ControllerPago extends AbstractControllerIngreso{

    public ControllerPago(Ingreso view, Pago model) {
        super(view, model);
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics grphcs, PageFormat pf, int i) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        if (!escribirDatosDB()) {
            Messages.errorMessage("No se pudo Guardar en la base de datos");
        }
    }

    @Override
    public boolean escribirDatosDB() {
        if (view.verificarDatos()){
            try {
                Pago pago = (Pago) view.getModelFromView();
                storedProcedure = conexion.prepareCall("call grabarPago(?,?,?)");
                storedProcedure.setInt(1, pago.getFactura().getIdFactura());
                storedProcedure.setString(2, pago.getDescripcion());                
                storedProcedure.setBigDecimal(3, pago.getMonto());
                resultSet = storedProcedure.executeQuery();
                if (pago.getIdPago()== 0 && resultSet.next()) {
                    pago.setIdPago(resultSet.getInt(1));
                }
            } catch (SQLException e) {
                System.out.println("Error:\n" + e.getMessage());
                return false;
            }
            view.Actualizar();
            return true;
        }
        return false;        
    }

    @Override
    public boolean leerDatosDB() {
        return false;
    }
    
}
