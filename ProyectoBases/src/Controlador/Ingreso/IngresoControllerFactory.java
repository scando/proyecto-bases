/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Ingreso;

import Controlador.Busqueda.ControllerFacturas;
import InterfacesAdministrador.ingresoDatos.*;
import Modelo.*;
/**
 *
 * @author Clases
 */
public class IngresoControllerFactory extends AbstractControllerFactory{
    
    private IngresoControllerFactory() {
    }
    
    public static IngresoControllerFactory getInstance() {
        return IngresoControllerFactoryHolder.INSTANCE;
    }

    @Override
    public AbstractControllerIngreso getFactory(String nombreControlador, String nombreVista, modelDataBaseInterface model) {
        AbstractControllerIngreso ver = null; 
        if (nombreControlador == null | nombreVista == null | model == null) {
            return null;
        }

        switch(nombreControlador){
            case "Usuario":{
                userNewPanel vista = (userNewPanel)IngresarFactory.getInstance().getIngresoDatos(nombreVista);
                Usuario modelo = (Usuario)model;
                ver = new ControllerUsuario(vista, modelo);
            }break;
            case "Ejercicio":{
                EjercicioNuevoPanel vista = (EjercicioNuevoPanel)IngresarFactory.getInstance().getIngresoDatos(nombreVista);
                Ejercicio modelo = (Ejercicio) model;
                ver = new ControllerEjercicio(vista, modelo);
            }break;
            case "Rutina":{
                //ruti vista = (EjercicioNuevoPanel)IngresarFactory.getInstance().getIngresoDatos(nombreVista);
                //Rutina modelo = (Rutina) model;
                //ver = new ControllerRutinas(vista, modelo);
            }break;
            case "Membresia": {
                TipoMembresiaNuevoPanel vista = (TipoMembresiaNuevoPanel)IngresarFactory.getInstance().getIngresoDatos(nombreVista);
                TipoMembresia modelo = (TipoMembresia) model;
                ver = new ControllerTipoMembresia(vista, modelo);
            }
            break;
            case "Factura": {
                facturaNewPanel vista = (facturaNewPanel)IngresarFactory.getInstance().getIngresoDatos(nombreVista);
                Factura modelo = (Factura) model;
                ver = new ControllerFactura(vista, modelo);
            }
            break;
            case "Dieta": {
                //EjercicioNuevoPanel vista = (EjercicioNuevoPanel)IngresarFactory.getInstance().getIngresoDatos(nombreVista);
                //Ejercicio modelo = (Ejercicio) model;
                //ver = new ControllerDieta(vista, modelo);
            }
            break;
            case "AreaDelCuerpo": {
                areaDelCuerpoPanel vista = (areaDelCuerpoPanel)IngresarFactory.getInstance().getIngresoDatos(nombreVista);
                AreaDelCuerpo modelo = (AreaDelCuerpo) model;
                ver = new ControllerAreaDelCuerpo(vista, modelo);
            }
            break;
        }

        return ver;
    }
    
    private static class IngresoControllerFactoryHolder {

        private static final IngresoControllerFactory INSTANCE = new IngresoControllerFactory();
    }
}
