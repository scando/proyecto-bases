/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Ingreso;

import Clases.Util.Messages;
import InterfacesAdministrador.ingresoDatos.ComidaNuevoPanel;
import Modelo.Comida;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;

/**
 *
 * @author Clases
 */
public class ControllerComida extends AbstractControllerIngreso{
  
    public ControllerComida(ComidaNuevoPanel vista, Comida modelo) {
        super(vista, modelo);
        
    }
    
    
    @Override
    public boolean escribirDatosDB() {      
        if (view.verificarDatos()){
            try {
                Comida comida = (Comida) view.getModelFromView();
                storedProcedure = conexion.prepareCall("call grabarComida(?,?,?,?,?,?)");
                storedProcedure.setInt(1, comida.getIdComida());
                storedProcedure.setInt(2, comida.getCalorias());
                storedProcedure.setString(3, comida.getNombre());
                storedProcedure.setString(4, comida.getDescripcion());
                storedProcedure.setBoolean(5, comida.isPlatoFuerte());
                storedProcedure.setInt(6, comida.getHora().getIdHoraComida());
                resultSet = storedProcedure.executeQuery();
                if (comida.getIdComida() == 0 && resultSet.next()) {
                    comida.setIdComida(resultSet.getInt(1));
                }
            } catch (SQLException e) {
                System.out.println("Error:\n" + e.getMessage());
                return false;
            }
            view.Actualizar();
            return true;
        }
        return false;
    }

    @Override
    public boolean leerDatosDB() {
        return false;
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        if (!escribirDatosDB()) {
            Messages.errorMessage("No se pudo Guardar en la base de datos");
        }
    }

}
