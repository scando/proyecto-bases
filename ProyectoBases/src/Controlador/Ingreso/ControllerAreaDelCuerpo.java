/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador.Ingreso;

import Clases.Util.Messages;
import InterfacesAdministrador.ingresoDatos.EjercicioNuevoPanel;
import InterfacesAdministrador.ingresoDatos.IngresarFactory;
import InterfacesAdministrador.ingresoDatos.Ingreso;
import InterfacesAdministrador.ingresoDatos.areaDelCuerpoPanel;
import Modelo.AreaDelCuerpo;
import Modelo.modelDataBaseInterface;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Maro
 */
public class ControllerAreaDelCuerpo extends AbstractControllerIngreso{

    public ControllerAreaDelCuerpo(areaDelCuerpoPanel view, AreaDelCuerpo model) {
        super(view, model);
    }

    @Override
    public boolean escribirDatosDB() {
        if (view.verificarDatos()){
            try {
                AreaDelCuerpo adc = (AreaDelCuerpo) view.getModelFromView();
                storedProcedure = conexion.prepareCall("call grabarADC(?,?)");
                storedProcedure.setInt(1, adc.getIdADC());
                storedProcedure.setString(2, adc.getNombre());
                resultSet = storedProcedure.executeQuery();
                if (adc.getIdADC()== 0 && resultSet.next()) {
                    adc.setIdADC(resultSet.getInt(1));
                }
            } catch (SQLException e) {
                System.out.println("Error:\n" + e.getMessage());
                return false;
            }
            view.Actualizar();
            return true;
        }
        return false;
    }

    @Override
    public boolean leerDatosDB() {
        return false;
    }

    @Override
    public void imprimir(int interfaz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save() {
        if (!escribirDatosDB()) {
            Messages.errorMessage("No se pudo Guardar en la base de datos");
        }
    }
    
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            
        } catch (InstantiationException ex) {
            
        } catch (IllegalAccessException ex) {
            
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame();
                IngresarFactory.getInstance().getIngresoDatos("Area del cuerpo");
                ControllerAreaDelCuerpo controlador = new ControllerAreaDelCuerpo((areaDelCuerpoPanel) IngresarFactory.getInstance().getIngresoDatos("AreaDelCuerpo"), new AreaDelCuerpo(0, null));
                frame.add((JPanel) controlador.getView());
                frame.pack();
                
                frame.setVisible(true);
                
            }
        });
    }
}
