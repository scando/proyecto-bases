/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Clases.InterfaceMethods.PrintInterface;
import Clases.InterfaceMethods.SaveInterface;
import InterfacesAdministrador.vtnNueva;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import javax.swing.JPanel;

/**
 *
 * @author Clases
 */
public abstract class AbstractController implements PrintInterface, SaveInterface{
    protected static boolean isConnected = ControladorConectar.getInstance().isConnected();
    
    protected Connection conexion;
    protected CallableStatement storedProcedure;
    protected ResultSet resultSet;
    
    public AbstractController() {
        this.conexion = ControladorConectar.getConexion();
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }
    
    
}
