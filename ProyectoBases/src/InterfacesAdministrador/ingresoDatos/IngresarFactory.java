/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.ingresoDatos;

import InterfacesAdministrador.AbstractFactory;
import InterfacesAdministrador.Busquedas.Busqueda;
import InterfacesAdministrador.reportes.Reportes;
import java.util.EventListener;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Clases
 */
public class IngresarFactory extends AbstractFactory{

    private static final IngresarFactory instance = new IngresarFactory();
    
    private IngresarFactory() {
    }

    public static IngresarFactory getInstance() {
        return instance;
    }
    
   
    @Override
    public Ingreso getIngresoDatos(String TipoIngresar) {
        Ingreso ingresar = null;
        
        if (TipoIngresar == null) {
            return ingresar;
        }

        switch(TipoIngresar){
            case "Persona":{
                ingresar = new userNewPanel();
            }break;
            case "Ejercicio":{
                ingresar = new EjercicioNuevoPanel();
            }break;
            case "Comida":{
                ingresar = new ComidaNuevoPanel();
            }break;
            case "Factura":{
                ingresar = new facturaNewPanel();
            }break;
            case "AreaDelCuerpo":{
                ingresar = new areaDelCuerpoPanel();
            }break;
            case "Membresia": {
                ingresar = new membresiaNuevoPanel();
            }
            break;
        }
        
        return ingresar;
    }

    @Override
    public Reportes getReportes(String TipoReportes) {
        return null;
    }

    @Override
    public Busqueda getBusqueda(String TipoBusqueda, EventListener controller, AbstractTableModel tableModel) {
        return null;
    }
    
}
