/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.ingresoDatos;

import Clases.InterfaceMethods.ConnectDbInterface;
import Modelo.modelDataBaseInterface;
import java.util.EventListener;

/**
 *
 * @author Clases
 */
public interface Ingreso {

    public boolean verificarDatos();
    public boolean datosVacios();
    
    public void setController(EventListener controller);
    
    public void setViewFromModel(modelDataBaseInterface model);
    public modelDataBaseInterface getModelFromView();
    
    public void Actualizar();
}