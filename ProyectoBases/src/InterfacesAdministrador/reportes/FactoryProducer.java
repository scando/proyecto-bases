/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.reportes;

/**
 *
 * @author Clases
 */
public class FactoryProducer {
    public static AbstractFactoryReportes getFactory(String choice){
   
      if(choice.equalsIgnoreCase("Estadisticas")){
         return new EstadisticasFactory();
         
      }else if(choice.equalsIgnoreCase("Membresias")){
         return new MembresiasFactory();
      }
      
      return null;
   }
}
