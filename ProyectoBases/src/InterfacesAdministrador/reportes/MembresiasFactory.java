/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.reportes;

/**
 *
 * @author Clases
 */
public class MembresiasFactory extends AbstractFactoryReportes{

    @Override
    Estadistica getEstadistica(String Estadistica) {
        return null;
    }

    @Override
    Membresia getMembresias(String Membresias) {
        if (Membresias == null) {
            return null;
        }

        if (Membresias.equalsIgnoreCase("Estado")) {
            return new MembresiaReportPanel();

        } else if (Membresias.equalsIgnoreCase("Usuario")) {
            return new UsuarioReportPanel();

        }

        return null;
    }
    
}
