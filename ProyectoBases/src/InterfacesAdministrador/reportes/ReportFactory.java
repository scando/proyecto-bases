/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.reportes;

import InterfacesAdministrador.AbstractFactory;
import InterfacesAdministrador.Busquedas.Busqueda;
import InterfacesAdministrador.ingresoDatos.Ingreso;
import java.util.EventListener;
import java.util.StringTokenizer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Clases
 */
public class ReportFactory extends AbstractFactory{

    private static final ReportFactory instance = new ReportFactory();

    private ReportFactory() {
    }

    public static ReportFactory getInstance() {
        return instance;
    }
    
    @Override
    public Busqueda getBusqueda(String TipoBusqueda, EventListener controller, AbstractTableModel tableModel) {
        return null;
    }
    
    
    @Override
    public Ingreso getIngresoDatos(String TipoIngresar) {
        return null;
    }

    /*
     * En el tipo de reporte se debe especificar con algun delimitador
     * el tipo de reporte que se quiere generar y de ahi el nombre dle
     * reporte
     */
    @Override
    public Reportes getReportes(String TipoReportes) {
        Reportes reporte = null;
        
        if (TipoReportes == null) {
            return reporte;
        }

        StringTokenizer token = new StringTokenizer(TipoReportes);
        String tipoReporte = token.nextToken();
        String nombreReporte = token.nextToken();
        
        switch(tipoReporte){
            case "Estadistica":{
                AbstractFactoryReportes estadistica = FactoryProducer.getFactory(tipoReporte);
                reporte = estadistica.getEstadistica(nombreReporte);
            }break;
            case "Membresia":{
                AbstractFactoryReportes membresia = FactoryProducer.getFactory(tipoReporte);
                reporte = membresia.getMembresias(nombreReporte);
            }break;
        }
        
        return reporte;
    }
}
