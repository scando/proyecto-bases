/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.reportes;

import java.awt.Rectangle;

/**
 *
 * @author Clases
 */
public class EstadisticasFactory extends AbstractFactoryReportes{

    @Override
    Estadistica getEstadistica(String Estadistica) {
        if (Estadistica == null) {
            return null;
        }

        if (Estadistica.equalsIgnoreCase("Ejercicio")) {
            return new EjercicioReportePanel();

        } else if (Estadistica.equalsIgnoreCase("Rutina")) {
            return new RutinaReporteBusqueda();

        } 

        return null;
    }

    @Override
    Membresia getMembresias(String Membresias) {
        return null;
    }
    
}
