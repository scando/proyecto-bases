/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador;

import Clases.Util.Messages;
import InterfacesAdministrador.Busquedas.BusquedaFactory;
import InterfacesAdministrador.ingresoDatos.IngresarFactory;
import InterfacesAdministrador.reportes.ReportFactory;

/**
 *
 * @author Clases
 */
public class FactoryProducer {
    public static AbstractFactory getFactory(String choice){
        AbstractFactory factory = null;
        
        switch(choice){
            case "Busqueda":{
                factory = BusquedaFactory.getInstance();
            }break;
            case "Ingreso":{
                factory = IngresarFactory.getInstance();
            }break;
            case "Reporte":{
                factory = ReportFactory.getInstance();
            }break;
            default:{
                System.out.println("No se encontro la fabrica a usar");
            }break;
        }
        
        return factory;
    }
}
