/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador;

import InterfacesAdministrador.reportes.Reportes;
import InterfacesAdministrador.ingresoDatos.Ingreso;
import InterfacesAdministrador.Busquedas.Busqueda;
import java.util.EventListener;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Clases
 */
public abstract class AbstractFactory {
    abstract public Busqueda getBusqueda(String TipoBusqueda, EventListener controller,AbstractTableModel tableModel);

    abstract public Ingreso getIngresoDatos(String TipoIngresar);
    abstract public Reportes getReportes (String TipoReportes);
}
