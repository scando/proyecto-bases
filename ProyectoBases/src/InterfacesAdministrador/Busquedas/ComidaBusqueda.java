/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.Busquedas;

import Clases.InterfaceMethods.ConnectDbInterface;
import Clases.Util.Messages;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusListener;
import java.util.EventListener;
import java.util.StringTokenizer;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Clases
 */
public class ComidaBusqueda extends AbstractDatosTablaBusqueda {

    public ComidaBusqueda(EventListener controller, AbstractTableModel tableModel) {
        super(controller, tableModel);
        initNewComponents();
        anadirModelo(model);
        anadirControlador(controller);
    }
        
    private void initNewComponents(){
        btnOpcion1.setText("Busqueda por nombre:");
        btnOpcion1.setToolTipText("Permite realizar busquedas por nombre");
        btnOpcion1.setActionCommand("Opcion1");
        
        
        btnOpcion2.setText("Busqueda por calorias");
        btnOpcion2.setToolTipText("Permite realizar busquedas por lugar de residencia");
        btnOpcion2.setActionCommand("Opcion2");
        
        btnOpcion3.setVisible(false);
    }    
    
    private String[] nombres;
    
    @Override
    public String[] buscar() {
        return nombres;
    }


    @Override
    public JTable getTable() {
        return TablaView;
    }

    @Override
    public void anadirModelo(TableModel modelo) {
        TablaView.setModel(modelo);
    }

    @Override
    public void anadirControlador(EventListener controlador) {
        txtBuscar.addFocusListener((FocusListener) controlador);
        btnOpcion1.addActionListener((ActionListener) controlador);
        btnOpcion2.addActionListener((ActionListener) controlador);
        setController(controller);
    }
    
}
