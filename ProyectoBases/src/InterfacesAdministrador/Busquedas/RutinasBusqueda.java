/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.Busquedas;

import Clases.InterfaceMethods.ConnectDbInterface;
import Clases.Util.Messages;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusListener;
import java.util.EventListener;
import java.util.StringTokenizer;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Clases
 */
public class RutinasBusqueda extends AbstractDatosTablaBusqueda{

    public RutinasBusqueda(EventListener controller, AbstractTableModel tableModel) {
        super(controller, tableModel);
        initNewComponents();
    }

    private void initNewComponents(){
             
        btnOpcion1.setText("Busqueda por año de inicio");
        btnOpcion1.setToolTipText("Permite realizar busqeuda de rutinas por año de inicio");
        btnOpcion1.setActionCommand("Opcion1");
        
   
    }
    
    private String[] nombres;
    
    @Override
    public String[] buscar() {
        return nombres;
    }
    
    @Override
    public JTable getTable() {
        return TablaView;
    }

    @Override
    public void anadirModelo(TableModel modelo) {
        TablaView.setModel(modelo);
    }

    @Override
    public void anadirControlador(EventListener controlador) {
        txtBuscar.addFocusListener((FocusListener) controlador);
        btnOpcion1.addActionListener((ActionListener) controlador);
    }

}
