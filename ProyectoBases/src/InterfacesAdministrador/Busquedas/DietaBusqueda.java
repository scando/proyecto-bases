/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.Busquedas;

import Clases.Util.Messages;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusListener;
import java.util.EventListener;
import java.util.StringTokenizer;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Clases
 */
public class DietaBusqueda extends AbstractDatosTablaBusqueda implements ActionListener{

    public DietaBusqueda(EventListener controller, AbstractTableModel tableModel) {
        super(controller, tableModel);
        initNewComponents();
    }

    
    private void initNewComponents(){
                
        btnOpcion1.setText("Busqueda por tipo de membresia:");
        btnOpcion1.setToolTipText("Permite realizar busquedas tipo de membresia");
        btnOpcion1.setActionCommand("Opcion1");
                
        btnOpcion2.setText("Busqueda por costo de membresia");
        btnOpcion2.setToolTipText("Permite realizar busquedas por costo de membresia");
        btnOpcion2.setActionCommand("Opcion2");
        
    }

    private String[] nombres;
    
        
    @Override
    public JTable getTable() {
        return TablaView;
    }

    @Override
    public void anadirModelo(TableModel modelo) {
        TablaView.setModel(modelo);
        
    }

    @Override
    public void anadirControlador(EventListener controlador) {
        txtBuscar.addFocusListener((FocusListener) controlador);
        btnOpcion1.addActionListener((ActionListener) controlador);
        btnOpcion2.addActionListener((ActionListener) controlador);
    }

    @Override
    public String[] buscar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
