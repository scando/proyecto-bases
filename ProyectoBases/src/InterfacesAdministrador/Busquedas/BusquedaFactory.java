/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.Busquedas;

import InterfacesAdministrador.AbstractFactory;
import InterfacesAdministrador.ingresoDatos.Ingreso;
import InterfacesAdministrador.reportes.Reportes;
import java.awt.event.FocusAdapter;
import java.util.EventListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Clases
 */
public class BusquedaFactory extends AbstractFactory{
    
    private static final BusquedaFactory instance = new BusquedaFactory();
    
    private BusquedaFactory() {
    }
    
    public static BusquedaFactory getInstance(){
        return instance;
    }
    
    @Override
    public Busqueda getBusqueda(String TipoBusqueda, EventListener controller,AbstractTableModel tableModel) {
        Busqueda buscar = null; 
        if (TipoBusqueda == null) {
            return null;
        }

        switch(TipoBusqueda){
            case "Usuario":{
                buscar = new UsuariosBusquedas(controller, tableModel);
            }break;
            case "Ejercicio":{
                buscar = new EjercicioBusqueda(controller, tableModel);
            }break;
            case "Rutina":{
                buscar = new RutinasBusqueda(controller, tableModel);
            }break;
            case "Membresia": {
                buscar = new MembresiasBusquedas(controller, tableModel);
            }
            break;
            case "Factura": {
                buscar = new FacturasBusquedas(controller, tableModel);
            }
            break;
            case "Dieta": {
                buscar = new DietaBusqueda(controller, tableModel);
            }
            break;
            case "AreaDelCuerpo": {
                buscar = new AreasDelCuerpoBusquedas(controller, tableModel);
            }
            break;
            default:{
                buscar = new BusquedaVacia(controller, tableModel);
            }break;
        }

        return buscar;
    }

    @Override
    public Ingreso getIngresoDatos(String TipoIngresar) {
        return null;
    }

    @Override
    public Reportes getReportes(String TipoReportes) {
        return null;
    }

   
}
