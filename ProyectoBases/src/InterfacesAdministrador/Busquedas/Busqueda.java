/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.Busquedas;

import java.awt.event.FocusAdapter;
import java.util.EventListener;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.table.TableModel;

/**
 *
 * @author Clases
 */
public interface Busqueda {
    /*
     * Retorna un listado con todas las palabras para generar la busqueda
     */
    public String[] buscar();
    public JTable getTable();
    public void anadirModelo(TableModel modelo);
    public void anadirControlador(EventListener controlador);
    
    public JPopupMenu getPopUpMenu();
}
