/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.Busquedas;

import Clases.InterfaceMethods.ConnectDbInterface;
import Clases.Util.Messages;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusListener;
import java.util.EventListener;
import java.util.StringTokenizer;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Clases
 */
public class EjercicioBusqueda extends AbstractDatosTablaBusqueda{

    public EjercicioBusqueda(EventListener controller, AbstractTableModel tableModel) {
        super(controller, tableModel);
        initNewComponents();
    }

    private void initNewComponents(){
        btnOpcion4 = new JButton();
        
        btnOpcion1.setText("Busqueda por nombre:");
        btnOpcion1.setToolTipText("Permite realizar busquedas por nombre");
        btnOpcion1.setActionCommand("Opcion1");
        
        
        btnOpcion2.setText("Busqueda por Descripcion");
        btnOpcion2.setToolTipText("Permite realizar busquedas por descripcion");
        btnOpcion2.setActionCommand("Opcion2");
        
        
        btnOpcion3.setText("Busqueda por area del cuerpo");
        btnOpcion3.setToolTipText("Permite realizar busquedas por areas del cuerpo");
        btnOpcion3.setActionCommand("Opcion3");
        
        
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 4;
        
        btnOpcion4.setText("Busqueda por Intensidad");
        btnOpcion4.setToolTipText("Permite realizar busquedas dada una intensidad");
        btnOpcion4.setActionCommand("Opcion4");
        OptionPanel.add(btnOpcion4,c);
        
    }
    
    private JButton btnOpcion4;
    private String[] nombres;
    
  

    @Override
    public String[] buscar() {
        return nombres;
    }

    @Override
    public JTable getTable() {
        return TablaView;
    }

    @Override
    public void anadirModelo(TableModel modelo) {
        TablaView.setModel(modelo);
        
    }

    @Override
    public void anadirControlador(EventListener controlador) {
        txtBuscar.addFocusListener((FocusListener) controlador);
        btnOpcion1.addActionListener((ActionListener) controlador);
        btnOpcion2.addActionListener((ActionListener) controlador);
        btnOpcion3.addActionListener((ActionListener) controlador);
        btnOpcion4.addActionListener((ActionListener) controlador);
    }

}
