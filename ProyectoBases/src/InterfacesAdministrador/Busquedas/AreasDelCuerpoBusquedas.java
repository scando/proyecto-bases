/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.Busquedas;

import Clases.Util.Messages;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusListener;
import java.util.EventListener;
import java.util.StringTokenizer;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Clases
 */
public class AreasDelCuerpoBusquedas extends AbstractDatosTablaBusqueda {

    public AreasDelCuerpoBusquedas(EventListener controller, AbstractTableModel tableModel) {
        super(controller, tableModel);
        initNewComponents();
    }

    private void initNewComponents(){
                
        btnOpcion1.setText("Busqueda por tipo de membresia:");
        btnOpcion1.setToolTipText("Permite realizar busquedas tipo de membresia");
        btnOpcion1.setActionCommand("Opcion1");
        
        btnOpcion2.setText("Busqueda por costo de membresia");
        btnOpcion2.setToolTipText("Permite realizar busquedas por costo de membresia");
        btnOpcion2.setActionCommand("Opcion2");
        
    }

    
    
    @Override
    public String[] buscar() {
        return null;
    }
    
    @Override
    public JTable getTable() {
        return TablaView;
    }

    @Override
    public void anadirModelo(TableModel modelo) {
       TablaView.setModel(modelo);
    }

    @Override
    public void anadirControlador(EventListener controlador) {
        txtBuscar.addFocusListener((FocusListener) controlador);
        btnOpcion1.addActionListener((ActionListener) controlador);
        btnOpcion2.addActionListener((ActionListener) controlador);
        setController(controller);
    }
}
