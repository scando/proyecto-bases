/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfacesAdministrador.Busquedas;

import Clases.InterfaceMethods.ConnectDbInterface;
import Clases.Util.Messages;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusListener;
import java.util.EventListener;
import java.util.StringTokenizer;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Clases
 */
public class MembresiasBusquedas extends AbstractDatosTablaBusqueda implements ActionListener{

    public MembresiasBusquedas(EventListener controller, AbstractTableModel tableModel) {
        super(controller, tableModel);
    }

    private void initNewComponents(){
                
        btnOpcion1.setText("Busqueda por tipo de membresia:");
        btnOpcion1.setToolTipText("Permite realizar busquedas tipo de membresia");
        btnOpcion1.setActionCommand("Opcion1");
        btnOpcion1.addActionListener(this);
        
        btnOpcion2.setText("Busqueda por costo de membresia");
        btnOpcion2.setToolTipText("Permite realizar busquedas por costo de membresia");
        btnOpcion2.setActionCommand("Opcion2");
        btnOpcion2.addActionListener(this);
    }

    private String[] nombres;
    
    @Override
    public void actionPerformed(ActionEvent e) {
        StringTokenizer token;
        String actionComand = e.getActionCommand();
        switch(actionComand){
            case "Opcion1":{
                String busNom = Messages.inputMessage("Ingrese tipo de membresia a buscar");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }
            }break;
            case "Opcion2":{
                String busNom = Messages.inputMessage("Ingrese el costo de la membresia");
                if(!busNom.isEmpty()){
                    token = new StringTokenizer(busNom,"|");
                    nombres = new String[token.countTokens()];
                    int i = 0;
                    while(token.hasMoreElements()){
                        nombres[i]=token.nextToken();
                        i++;
                    }
                }                
            }break;
        }        
    }

    @Override
    public String[] buscar() {
        return nombres;
    }
    
    @Override
    public JTable getTable() {
        return TablaView;
    }

    @Override
    public void anadirModelo(TableModel modelo) {
        TablaView.setModel(modelo);
    }


    @Override
    public void anadirControlador(EventListener controlador) {
        
        txtBuscar.addFocusListener((FocusListener) controlador);
    }

}
