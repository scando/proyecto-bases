/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;

/**
 *
 * @author Clases
 */
public class UsuarioDieta implements modelDataBaseInterface{
    private int idUsuarioDieta;
    private int idDieta;
    private int idUsuario;
    private Date FechaInicio;
    private Date FechaFin;

    public UsuarioDieta(int idUsuarioDieta, int idDieta, int idUsuario, Date FechaInicio, Date FechaFin) {
        this.idUsuarioDieta = idUsuarioDieta;
        this.idDieta = idDieta;
        this.idUsuario = idUsuario;
        this.FechaInicio = FechaInicio;
        this.FechaFin = FechaFin;
    }

    public int getIdUsuarioDieta() {
        return idUsuarioDieta;
    }

    public void setIdUsuarioDieta(int idUsuarioDieta) {
        this.idUsuarioDieta = idUsuarioDieta;
    }

    public int getIdDieta() {
        return idDieta;
    }

    public void setIdDieta(int idDieta) {
        this.idDieta = idDieta;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFechaInicio() {
        return FechaInicio;
    }

    public void setFechaInicio(Date FechaInicio) {
        this.FechaInicio = FechaInicio;
    }

    public Date getFechaFin() {
        return FechaFin;
    }

    public void setFechaFin(Date FechaFin) {
        this.FechaFin = FechaFin;
    }

    
}
