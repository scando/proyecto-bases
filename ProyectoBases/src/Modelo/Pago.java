/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.math.BigDecimal;
import java.sql.Date;

/**
 *
 * @author Juan
 */
public class Pago implements modelDataBaseInterface{

    private int idPago;
    private Date fecha;    
    private Factura factura;
    private String descripcion;
    private BigDecimal monto;
    private boolean valido;

    public int getIdPago() {
        return idPago;
    }

    public void setIdPago(int idPago) {
        this.idPago = idPago;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public boolean isValido() {
        return valido;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }

    public Pago(int idPago, Date fecha, Factura factura, String descripcion, BigDecimal monto, boolean valido) {
        this.idPago = idPago;
        this.fecha = fecha;
        this.factura = factura;
        this.descripcion = descripcion;
        this.monto = monto;
        this.valido = valido;
    }
}
