/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Busqueda;

import Clases.Util.Messages;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Clases
 */
public class ResultSetTableModel extends AbstractTableModel{

    private ResultSet resultSet;
    private ResultSetMetaData metaData;
    private int numberOfRows;
    private Boolean connectedToDatabase;
   
    public ResultSetTableModel(ResultSet resultSet, Boolean connectedToDatabase) throws SQLException {
        this.connectedToDatabase = connectedToDatabase;
        setResultSet(resultSet);
    }

    public void setConnectedToDatabase(Boolean connectedToDatabase) {
        this.connectedToDatabase = connectedToDatabase;
    }

    public void setResultSet(ResultSet resultSet) throws SQLException {
        if(resultSet != null){
            this.resultSet = resultSet;
            changeValues();
        }
    }

    
    public void changeValues() throws SQLException{
        this.metaData = resultSet.getMetaData();
        
        resultSet.last();
        numberOfRows = resultSet.getRow();
        
        fireTableStructureChanged();
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if(!connectedToDatabase)
            Messages.errorMessage("No esta conectado con la base de datos");
        try{
            String className = metaData.getColumnClassName(columnIndex + 1);
            return Class.forName(className);
        }catch(SQLException | ClassNotFoundException exception){
            
        }
        return Object.class;
    }
    
    @Override
    public int getRowCount() {
        if(!connectedToDatabase)
            Messages.errorMessage("No esta conectado con la base de datos");
        return numberOfRows;
    }

    @Override
    public int getColumnCount() {
        if(!connectedToDatabase)
            Messages.errorMessage("No esta conectado con la base de datos");
        try{
            return metaData.getColumnCount();
        }catch( SQLException sQLException){
        }
        
        return 0;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(!connectedToDatabase)
            ;
        try{
            resultSet.absolute( rowIndex + 1 );
            return resultSet.getObject( columnIndex + 1 );
        }catch(SQLException sqle){
            
        }
        return "";
    }

    @Override
    public String getColumnName(int column) {
        if(!connectedToDatabase)
            Messages.errorMessage("No esta conectado con la base de datos");
        try{
            return metaData.getColumnName( column + 1 );
        }catch(SQLException sqle){
            
        }
        return "";
    }
    
    
}
