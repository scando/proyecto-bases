/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observer;

/**
 *
 * @author Juan
 */
public class Usuario implements modelDataBaseInterface {

    private int idUsuario;
    private String nombre;
    private String cedula;
    private String userName;
    private String contrasena;    
    private String email;
    private String residencia;
    private String pais;
    private String ciudad;
    private int pesoInicial;
    private int pesoActual;
    private int pesoMeta;
    private int estatura;
    private boolean perfilPublico;
    private TipoMembresia tipoMembresia;
    private Date fechaInicio;
    private Date fechaFin;

    public Usuario(int idUsuario, String nombre, String cedula, String userName, String contrasena, String email, String residencia, String pais, String ciudad, int pesoInicial, int pesoActual, int pesoMeta, int estatura, boolean perfilPublico, TipoMembresia tipoMembresia, Date fechaInicio, Date fechaFin) {
        this.idUsuario = idUsuario;
        this.nombre = nombre;
        this.userName = userName;
        this.contrasena = contrasena;
        this.cedula = cedula;
        this.email = email;
        this.residencia = residencia;
        this.pais = pais;
        this.ciudad = ciudad;
        this.pesoInicial = pesoInicial;
        this.pesoActual = pesoActual;
        this.pesoMeta = pesoMeta;
        this.estatura = estatura;
        this.perfilPublico = perfilPublico;
        this.tipoMembresia = tipoMembresia;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResidencia() {
        return residencia;
    }

    public void setResidencia(String residencia) {
        this.residencia = residencia;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getPesoInicial() {
        return pesoInicial;
    }

    public void setPesoInicial(int pesoInicial) {
        this.pesoInicial = pesoInicial;
    }

    public int getPesoActual() {
        return pesoActual;
    }

    public void setPesoActual(int pesoActual) {
        this.pesoActual = pesoActual;
    }

    public int getPesoMeta() {
        return pesoMeta;
    }

    public void setPesoMeta(int pesoMeta) {
        this.pesoMeta = pesoMeta;
    }

    public int getEstatura() {
        return estatura;
    }

    public void setEstatura(int estatura) {
        this.estatura = estatura;
    }

    public boolean isPerfilPublico() {
        return perfilPublico;
    }

    public void setPerfilPublico(boolean perfilPublico) {
        this.perfilPublico = perfilPublico;
    }

    public TipoMembresia getTipoMembresia() {
        return tipoMembresia;
    }

    public void setTipoMembresia(TipoMembresia tipoMembresia) {
        this.tipoMembresia = tipoMembresia;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

}
