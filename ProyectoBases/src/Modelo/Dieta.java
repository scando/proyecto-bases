/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Time;
import java.util.ArrayList;

/**
 *
 * @author Juan
 */
public class Dieta implements modelDataBaseInterface{

    private int idDieta;    
    private String nombre;
    private int ingestaCalorica;

    public Dieta(int idDieta, String nombre, int ingestaCalorica) {
        this.idDieta = idDieta;
        this.nombre = nombre;
        this.ingestaCalorica = ingestaCalorica;
    }

    public int getIdDieta() {
        return idDieta;
    }

    public void setIdDieta(int idDieta) {
        this.idDieta = idDieta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIngestaCalorica() {
        return ingestaCalorica;
    }

    public void setIngestaCalorica(int ingestaCalorica) {
        this.ingestaCalorica = ingestaCalorica;
    }
}
