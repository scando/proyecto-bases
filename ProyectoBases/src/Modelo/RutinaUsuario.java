/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;

/**
 *
 * @author Clases
 */
public class RutinaUsuario implements modelDataBaseInterface{
    private int idRutinaUsuario;
    private int idRutina;
    private int idUsuario;
    private Date FechaInicio;
    private Date FechaFin;

    public RutinaUsuario(int idRutinaUsuario, int idRutina, int idUsuario, Date FechaInicio, Date FechaFin) {
        this.idRutinaUsuario = idRutinaUsuario;
        this.idRutina = idRutina;
        this.idUsuario = idUsuario;
        this.FechaInicio = FechaInicio;
        this.FechaFin = FechaFin;
    }

    public int getIdRutinaUsuario() {
        return idRutinaUsuario;
    }

    public void setIdRutinaUsuario(int idRutinaUsuario) {
        this.idRutinaUsuario = idRutinaUsuario;
    }

    public int getIdRutina() {
        return idRutina;
    }

    public void setIdRutina(int idRutina) {
        this.idRutina = idRutina;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFechaInicio() {
        return FechaInicio;
    }

    public void setFechaInicio(Date FechaInicio) {
        this.FechaInicio = FechaInicio;
    }

    public Date getFechaFin() {
        return FechaFin;
    }

    public void setFechaFin(Date FechaFin) {
        this.FechaFin = FechaFin;
    }

}
