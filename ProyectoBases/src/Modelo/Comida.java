/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Juan
 */
public class Comida implements modelDataBaseInterface{

    private int idComida;
    private int calorias;
    private String nombre;
    private String descripcion;
    private boolean platoFuerte;
    private HoraComida hora;

    public Comida(int idComida, int calorias, String nombre, String descripcion, boolean platoFuerte, HoraComida hora) {
        this.idComida = idComida;
        this.calorias = calorias;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.platoFuerte = platoFuerte;
        this.hora = hora;
    }

    public int getIdComida() {
        return idComida;
    }

    public void setIdComida(int idComida) {
        this.idComida = idComida;
    }

    public int getCalorias() {
        return calorias;
    }

    public void setCalorias(int calorias) {
        this.calorias = calorias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isPlatoFuerte() {
        return platoFuerte;
    }

    public void setPlatoFuerte(boolean platoFuerte) {
        this.platoFuerte = platoFuerte;
    }

    public HoraComida getHora() {
        return hora;
    }

    public void setHora(HoraComida hora) {
        this.hora = hora;
    }

    
}
