/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 *
 * @author Maro
 */
public class HoraComida implements modelDataBaseInterface{
    private int idHoraComida;
    private String nombre;

    @Override
    public String toString() {
        return nombre;
    }
    
    public HoraComida(int idHoraComida, String nombre) {
        this.idHoraComida = idHoraComida;
        this.nombre = nombre;
    }

    public int getIdHoraComida() {
        return idHoraComida;
    }

    public void setIdHoraComida(int idHoraComida) {
        this.idHoraComida = idHoraComida;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

   
    
}
