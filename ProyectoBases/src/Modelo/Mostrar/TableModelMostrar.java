/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Mostrar;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Clases
 */
public  class TableModelMostrar extends AbstractTableModel{

    private String[] columnNames = {
        "TEAM", "P", "W", "D", "L", "GS", "GA", "GD", "PTS"
    };
    // TableModel's data
    private Object[][] data = {
        {"Chelsea", 5, 5, 0, 0, 21, 1, 20, 15},
        {"Arsenal", 5, 3, 2, 0, 14, 4, 10, 11},
        {"Manchester United", 5, 3, 2, 0, 14, 7, 7, 11},
        {"Manchester City", 5, 2, 2, 1, 6, 2, 4, 8},
        {"Tottenham Hotspur", 5, 2, 2, 1, 6, 4, 2, 8}
    };
    
    private boolean editable;

    public TableModelMostrar(boolean editable) {
        this.editable = editable;
    }
    
    
    @Override
    public int getRowCount() {
        return data.length;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data[rowIndex][columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    } 
}
