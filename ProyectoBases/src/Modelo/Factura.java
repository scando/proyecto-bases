/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;
import java.math.BigDecimal;

/**
 *
 * @author Juan
 */

public class Factura implements modelDataBaseInterface{

    private int idFactura;
    private Date fechaEmision;
    private Date fechaCredito;
    private BigDecimal monto;
    private Usuario usuario;
    private boolean valido;
    private BigDecimal saldo;

    public Factura(int idFactura, Date fechaEmision, Date fechaCredito, BigDecimal monto, Usuario usuario, boolean valido, BigDecimal saldo) {
        this.idFactura = idFactura;
        this.fechaEmision = fechaEmision;
        this.fechaCredito = fechaCredito;
        this.monto = monto;
        this.usuario = usuario;
        this.valido = valido;
        this.saldo = saldo;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaCredito() {
        return fechaCredito;
    }

    public void setFechaCredito(Date fechaCredito) {
        this.fechaCredito = fechaCredito;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public boolean isValido() {
        return valido;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    
}
