/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Clases
 */
public class Intensidad implements modelDataBaseInterface{
    private int idIntensidad;
    private String nombre;

    @Override
    public String toString() {
        return nombre;
    }

    public Intensidad(int idIntensidad, String nombre) {
        this.idIntensidad = idIntensidad;
        this.nombre = nombre;
    }
    
    public int getIdIntensidad() {
        return idIntensidad;
    }

    public void setIdIntensidad(int idIntensidad) {
        this.idIntensidad = idIntensidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
}
