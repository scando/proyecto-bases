/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.math.BigDecimal;
/**
 *
 * @author Clases
 */
public class TipoMembresia implements modelDataBaseInterface{
    private int idTipoMembresia;
    private String nombreTipoMembresia;
    private int diasVigencia;
    private BigDecimal Costo;

    public TipoMembresia(int idTipoMembresia, String nombreTipoMembresia, int diasVigencia, BigDecimal Costo) {
        this.idTipoMembresia = idTipoMembresia;
        this.nombreTipoMembresia = nombreTipoMembresia;
        this.diasVigencia = diasVigencia;
        this.Costo = Costo;
    }

   

    public int getIdTipoMembresia() {
        return idTipoMembresia;
    }

    public void setIdTipoMembresia(int idTipoMembresia) {
        this.idTipoMembresia = idTipoMembresia;
    }

    public String getNombreTipoMembresia() {
        return nombreTipoMembresia;
    }

    public void setNombreTipoMembresia(String nombreTipoMembresia) {
        this.nombreTipoMembresia = nombreTipoMembresia;
    }

    public int getDiasVigencia() {
        return diasVigencia;
    }

    public void setDiasVigencia(int diasVigencia) {
        this.diasVigencia = diasVigencia;
    }

    public BigDecimal getCosto() {
        return Costo;
    }

    public void setCosto(BigDecimal Costo) {
        this.Costo = Costo;
    }
    
}
